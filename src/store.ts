import { init, RematchRootState, RematchStore } from '@rematch/core'
import createRematchPersist from '@rematch/persist'
import * as models from './models'

const persistPlugin = createRematchPersist({
  key: 'lukas-strv-test-project',
})

export type Models = typeof models

const store: RematchStore<Models> = init<Models>({
  models,
  plugins: [persistPlugin],
})

export default store

export type Store = typeof store
export type Dispatch = typeof store.dispatch
export type State = RematchRootState<Models>
