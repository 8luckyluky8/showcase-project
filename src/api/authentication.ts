import { ErrorResponse } from '.'
import { User } from 'src/models/user'
import { credentials, loginError } from './__mock__/authentication'
import { activeUser } from './__mock__/user'

export interface AuthenticationErrorResponse extends ErrorResponse {}

export type AuthenticationSuccessResponse = User

type AuthenticationResponse =
  | AuthenticationSuccessResponse
  | AuthenticationErrorResponse

export type AuthenticationRequestResult =
  | AuthenticationSuccessResponse
  | boolean

export const sendAuthenticationRequest = async (
  email: string,
  password: string
): Promise<AuthenticationRequestResult> => {
  const [mockEmail, mockPassword] = credentials

  const response: AuthenticationResponse = await new Promise<
    AuthenticationResponse
  >(resolve => {
    if (email === mockEmail && password === mockPassword) {
      resolve(activeUser)
    } else {
      resolve(loginError)
    }
  })

  if ((response as AuthenticationErrorResponse).error) {
    return false
  }

  return {
    ...response,
  } as AuthenticationSuccessResponse
}
