import { User } from 'src/models/user'

export const activeUser: User = {
  id: 1,
  firstName: 'Foo',
  lastName: 'Bar',
  email: 'foo@bar.com',
  authToken: 'lorem',
  refreshToken: 'ipsum',
}

export const darthVader: User = {
  id: 2,
  firstName: 'Darth',
  lastName: 'Vader',
  email: 'vader@death-star.com',
  authToken: 'darth',
  refreshToken: 'vader',
}

export const lukeSkywalker: User = {
  id: 3,
  firstName: 'Luke',
  lastName: 'Skywalker',
  email: 'Skywalker@rebellion.com',
  authToken: 'luke',
  refreshToken: 'skywalker',
}
