import { AuthenticationErrorResponse } from '../authentication'

export const credentials = ['foo@bar.com', '12345']

export const loginError: AuthenticationErrorResponse = {
  error: 'Incorrect credentials',
}
