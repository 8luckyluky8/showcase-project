import { EventData } from 'src/models/events'
import { lukeSkywalker, darthVader, activeUser } from './user'
import { ErrorResponse } from '..'

export let events: EventData[] = [
  {
    id: 1,
    owner: darthVader,
    attendees: [darthVader, lukeSkywalker],
    createdAt: 'Sun Jun 24 2018 10:00:00 GMT+0200 (CEST)',
    updatedAt: 'Sun Jun 24 2018 10:00:00 GMT+0200 (CEST)',
    title: "Father's day",
    description: 'Father and son reunion',
    startsAt: 'Sun Jun 25 2018 20:06:00 GMT+0200 (CEST)',
    capacity: 3,
  },
  {
    id: 2,
    owner: activeUser,
    attendees: [activeUser],
    createdAt: 'Sun Nov 25 2018 20:10:00 GMT+0200 (CEST)',
    updatedAt: 'Sun Nov 25 2018 20:10:00 GMT+0200 (CEST)',
    title: 'Xmass Party',
    description: "Let's celebrate christmass!",
    startsAt: 'Mon Dec 24 2018 19:00:00 GMT+0200 (CEST)',
    capacity: 10,
  },
]

export const updateEvents = (updatedEvents: EventData[]) => {
  events = updatedEvents
}

export const attendEventDuplicitAttendeeError: ErrorResponse = {
  error: 'User is already attending the event',
}

export const eventNotFoundError: ErrorResponse = {
  error: 'Event not found',
}

export const eventFullError: ErrorResponse = {
  error: 'Event is at full capacity',
}

export const unattendEventUerNotFoundError: ErrorResponse = {
  error: 'User is not attending the event',
}
