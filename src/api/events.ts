import { ErrorResponse } from '.'
import { EventData, EventFormData } from 'src/models/events'
import {
  events,
  eventNotFoundError,
  attendEventDuplicitAttendeeError,
  unattendEventUerNotFoundError,
  eventFullError,
  updateEvents,
} from './__mock__/events'
import { User } from 'src/models/user'
import { activeUser } from './__mock__/user'

export type AllEventsSuccessResponse = EventData[]

export type AllEventsResponse = AllEventsSuccessResponse

export const sendListAllEventsRequest = async (): Promise<
  AllEventsResponse
> => {
  const response = await new Promise<EventData[]>(resolve => {
    resolve([...events])
  })

  return response
}

export type AttendEventSuccessResponse = EventData

export type AttendEventResponse = AttendEventSuccessResponse | ErrorResponse

export const sendAttendEventRequest = async (
  eventId: EventData['id']
): Promise<AttendEventResponse> => {
  const response = await new Promise<AttendEventResponse>(resolve => {
    const event = events.find((event: EventData) => event.id === eventId)
    if (!event) {
      resolve(eventNotFoundError)
      return
    }

    if (event.capacity <= event.attendees.length) {
      resolve(eventFullError)
      return
    }

    const isDuplicitAttendee = event.attendees.some(
      (attendee: User) => attendee.id === activeUser.id
    )

    if (isDuplicitAttendee) {
      resolve(attendEventDuplicitAttendeeError)
    } else {
      const eventData: EventData = {
        ...event,
        attendees: [...event.attendees, activeUser],
      }

      updateEvents(
        events.map((event: EventData) => {
          if (event.id !== eventId) {
            return event
          }

          return eventData
        })
      )

      resolve(eventData)
    }
  })

  return response
}

export type UnattendEventSuccessResponse = EventData

export type UnattendEventResponse = UnattendEventSuccessResponse | ErrorResponse

export const sendUnattendEventRequest = async (
  eventId: number
): Promise<UnattendEventResponse> => {
  const response = await new Promise<AttendEventResponse>(resolve => {
    const event = events.find((event: EventData) => event.id === eventId)
    if (!event) {
      resolve(eventNotFoundError)
    } else {
      const isUserAttending = event.attendees.some(
        (attendee: User) => attendee.id === activeUser.id
      )

      if (!isUserAttending) {
        resolve(unattendEventUerNotFoundError)
      } else {
        const eventData: EventData = {
          ...event,
          attendees: event.attendees.filter(
            (attendee: User) => attendee.id !== activeUser.id
          ),
        }

        updateEvents(
          events.map((event: EventData) => {
            if (event.id !== eventId) {
              return event
            }

            return eventData
          })
        )

        resolve(eventData)
      }
    }
  })

  return response
}

export type CreateEventSuccessResponse = EventData

export type CreateEventResponse = CreateEventSuccessResponse

export const sendCreateEventRequest = async (
  eventFormData: EventFormData
): Promise<CreateEventResponse> => {
  const result: CreateEventResponse = await new Promise<CreateEventResponse>(
    resolve => {
      const id = events[events.length - 1].id + 1
      const today = new Date().toISOString()

      const eventData: EventData = {
        ...eventFormData,
        id,
        owner: activeUser,
        attendees: [activeUser],
        createdAt: today,
        updatedAt: today,
      }

      updateEvents([...events, eventData])

      resolve(eventData)
    }
  )

  return result
}

export type UpdateEventSuccessResponse = EventData

export type UpdateEventResponse = UpdateEventSuccessResponse | ErrorResponse

export const sendUpdateEventRequest = async (
  eventId: EventData['id'],
  eventFormData: EventFormData
): Promise<UpdateEventResponse> => {
  const result: UpdateEventResponse = await new Promise<UpdateEventResponse>(
    resolve => {
      const oldEventData: EventData | undefined = events.find(
        (event: EventData) => event.id === eventId
      )
      if (!oldEventData) {
        resolve(eventNotFoundError)
      }

      const today = new Date().toISOString()

      const { id, owner, attendees, createdAt } = oldEventData as EventData
      const { title, description, startsAt, capacity } = eventFormData

      // TS bug - multiple spread operators dont work
      const eventData: EventData = {
        id,
        owner,
        attendees,
        createdAt,
        title,
        description,
        startsAt,
        capacity,
        updatedAt: today,
      }

      console.log(eventData)

      updateEvents(
        events.map((event: EventData) => {
          if (event.id !== eventId) {
            return event
          }

          return eventData
        })
      )

      resolve(eventData)
    }
  )

  return result
}
