import * as React from 'react'
import * as ReactDOM from 'react-dom'
import AppRouter from './AppRouter'
import registerServiceWorker from './registerServiceWorker'
import './index.css'
import { Provider } from 'react-redux'
import store from './store'
import { persistStore } from 'redux-persist'

persistStore(store)

ReactDOM.render(
  <Provider store={store}>
    <AppRouter />
  </Provider>,
  document.getElementById('root') as HTMLElement
)
registerServiceWorker()
