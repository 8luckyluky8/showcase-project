import * as React from 'react'
import StyledInput from '../inputs/StyledInput'
import Heading from '../text/Heading'
import styled from 'styled-components'
import ErrorText from '../text/ErrorText'
import {
  sendAuthenticationRequest,
  AuthenticationSuccessResponse,
} from '../../api/authentication'
import ShowSecretIcon from '../icons/ShowSecretIcon'
import { media } from '../../theme'
import SignUpPrompt from '../ui/SignUpPrompt'
import { User } from '../../models/user'
import FormContainer from './FormContainer'
import InputContainer from '../inputs/InputContainer'
import PositiveLoadingButton from '../buttons/PositiveLoadingButton'
import FormPrimaryText from '../text/FormPrimaryText'

const ShowPasswordIcon = styled(ShowSecretIcon)`
  position: absolute;
  right: 2px;
  bottom: 0.8rem;
  cursor: pointer;
`

const MobileSignUpPrompt = styled(SignUpPrompt)`
  display: block;
  margin-bottom: 40px;
  ${/* sc-declaration */ media.desktop`
    display: none;
  `}
`

interface SignInFormProps {
  onLoggedIn: (user: User) => any
  onError?: () => any
}

const SignInForm = ({ onLoggedIn, onError }: SignInFormProps) => {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [isPasswordVisible, setIsPasswordVisible] = React.useState(false)
  const [hasError, setHasError] = React.useState(false)
  const [isInvalid, setIsInvalid] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)

  const onSubmit = async (event: React.SyntheticEvent) => {
    event.preventDefault()

    try {
      setIsLoading(true)
      const result = await sendAuthenticationRequest(email, password)
      setIsLoading(false)

      if (!result) {
        setIsInvalid(true)
        return
      }

      onLoggedIn(result as AuthenticationSuccessResponse)
    } catch {
      setIsLoading(false)
      setHasError(true)
      onError && onError()
    }
  }

  const clearError = () => {
    if (hasError) {
      setHasError(false)
    }
  }

  const clearInvalid = () => {
    if (isInvalid) {
      setIsInvalid(false)
    }
  }

  const clearErrors = () => {
    clearError()
    clearInvalid()
  }

  const handleEmailChange = (email: string) => {
    clearErrors()
    setEmail(email)
  }

  const handlePasswordChange = (password: string) => {
    clearErrors()
    setPassword(password)
  }

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible)
  }

  return (
    <FormContainer onSubmit={onSubmit}>
      <Heading>Sign in to Eventio.</Heading>

      {!hasError && !isInvalid && (
        <FormPrimaryText>Enter your details below:</FormPrimaryText>
      )}

      {isInvalid && (
        <ErrorText>
          Oops! That email and password combination is not valid.
        </ErrorText>
      )}

      {hasError && (
        <ErrorText>
          Oops! There has been some technical issue. Please try again later.
        </ErrorText>
      )}

      <InputContainer>
        <StyledInput
          value={email}
          onChange={handleEmailChange}
          onFocus={clearErrors}
          placeholder="Email"
          invalid={isInvalid}
        />

        <StyledInput
          value={password}
          onChange={handlePasswordChange}
          onFocus={clearErrors}
          placeholder="Password"
          type={isPasswordVisible ? 'text' : 'password'}
          invalid={isInvalid}
          renderIcon={() => (
            <ShowPasswordIcon onClick={togglePasswordVisibility} />
          )}
        />
      </InputContainer>

      <MobileSignUpPrompt />

      <PositiveLoadingButton
        type="submit"
        text="SIGN IN"
        isLoading={isLoading}
      />
    </FormContainer>
  )
}

export default SignInForm
