import * as React from 'react'
import FormContainer from './FormContainer'
import Heading from '../text/Heading'
import PrimaryText from '../text/PrimaryText'
import InputContainer from '../inputs/InputContainer'
import StyledInput from '../inputs/StyledInput'
import PositiveLoadingButton from '../buttons/PositiveLoadingButton'
import { EventFormData } from 'src/models/events'

interface EventFormProps {
  onSubmit: (eventFormData: EventFormData) => any
  isLoading?: boolean
  invalidInputs?: string[]
  defaultData?: EventFormData
}

const EventForm = ({
  onSubmit,
  isLoading,
  invalidInputs,
  defaultData,
}: EventFormProps) => {
  const [title, setTitle] = React.useState(defaultData ? defaultData.title : '')
  const [description, setDescription] = React.useState(
    defaultData ? defaultData.description : ''
  )
  const [date, setDate] = React.useState(
    defaultData ? new Date(defaultData.startsAt).toISOString().slice(0, 10) : ''
  )
  const [time, setTime] = React.useState(
    defaultData ? new Date(defaultData.startsAt).toLocaleTimeString() : ''
  )
  const [capacity, setCapacity] = React.useState(
    defaultData ? `${defaultData.capacity}` : ''
  )
  const [isInvalidDateTime, setIsInvalidDateTime] = React.useState(false)

  const onFormSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault()

    try {
      const startsAt = new Date(`${date} ${time}`).toISOString()

      onSubmit({
        title,
        description,
        startsAt,
        capacity: Number(capacity),
      })
    } catch {
      setIsInvalidDateTime(true)
    }
  }

  const handleDateChange = (value: string) => {
    setIsInvalidDateTime(false)
    setDate(value)
  }

  const handleTimeChange = (value: string) => {
    setIsInvalidDateTime(false)
    setTime(value)
  }

  const inInputInvalid = (inputKey: string) =>
    invalidInputs &&
    invalidInputs.length > 0 &&
    invalidInputs.some((errorInputKey: string) => errorInputKey === inputKey)

  return (
    <FormContainer onSubmit={onFormSubmit}>
      <Heading>{defaultData ? 'Edit an event.' : 'Create an event.'}</Heading>

      <PrimaryText>Enter details below.</PrimaryText>

      <InputContainer>
        <StyledInput
          value={title}
          onChange={setTitle}
          placeholder="Title"
          invalid={inInputInvalid('title')}
        />

        <StyledInput
          value={description}
          onChange={setDescription}
          placeholder="Description"
          invalid={inInputInvalid('description')}
        />

        <StyledInput
          type="date"
          value={date}
          onChange={handleDateChange}
          placeholder="Date"
          invalid={isInvalidDateTime || inInputInvalid('startsAt')}
        />

        <StyledInput
          type="time"
          value={time}
          onChange={handleTimeChange}
          placeholder="Time"
          invalid={isInvalidDateTime || inInputInvalid('startsAt')}
        />

        <StyledInput
          type="number"
          value={capacity}
          onChange={setCapacity}
          placeholder="Capacity"
          invalid={inInputInvalid('capacity')}
        />
      </InputContainer>

      <PositiveLoadingButton
        type="submit"
        text={defaultData ? 'EDIT EVENT' : 'CREATE NEW EVENT'}
        isLoading={isLoading}
      />
    </FormContainer>
  )
}

export default EventForm
