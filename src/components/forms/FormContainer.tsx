import styled from 'styled-components'
import { media } from '../../theme'

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  ${/* sc-declaration */ media.desktop`
    justify-content: center;
  `}
`

export default FormContainer
