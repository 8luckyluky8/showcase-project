import * as React from 'react'
import { shallow } from 'enzyme'
import FormContainer from '../FormContainer'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<FormContainer />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<FormContainer />).toJSON()

  expect(tree).toMatchSnapshot()
})
