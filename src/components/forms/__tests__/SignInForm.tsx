import * as React from 'react'
import SignInForm from '../SignInForm'
import * as renderer from 'react-test-renderer'

// @TODO: Fix tests after shallow rendering fix (probably related to https://github.com/facebook/react/issues/14091)

// it('renders without crashing', () => {
//   shallow(<SignInForm onLoggedIn={() => null} />);
// });

it('matches snapshot', () => {
  const tree = renderer.create(<SignInForm onLoggedIn={() => null} />).toJSON()

  expect(tree).toMatchSnapshot()
})
