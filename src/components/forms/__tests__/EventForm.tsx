import * as React from 'react'
import EventForm from '../EventForm'
import * as renderer from 'react-test-renderer'

// @TODO: Fix tests after shallow rendering fix (probably related to https://github.com/facebook/react/issues/14091)

// it('renders without crashing', () => {
//   shallow(<EventForm onSubmit={() => null} />);
// });

it('matches snapshot', () => {
  const tree = renderer.create(<EventForm onSubmit={() => null} />).toJSON()

  expect(tree).toMatchSnapshot()
})

// it('loading renders without crashing', () => {
//   shallow(<EventForm onSubmit={() => null} isLoading />);
// });

it('loading matches snapshot', () => {
  const tree = renderer
    .create(<EventForm onSubmit={() => null} isLoading />)
    .toJSON()

  expect(tree).toMatchSnapshot()
})

// it('renders without crashing with invalid inputs', () => {
//   shallow(<EventForm onSubmit={() => null} invalidInputs={['startsAt', 'title']} />);
// });

it('matches snapshot with invalid inputs', () => {
  const tree = renderer
    .create(
      <EventForm onSubmit={() => null} invalidInputs={['startsAt', 'title']} />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})

// it('loading renders without crashing with invalid inputs', () => {
//   shallow(<EventForm onSubmit={() => null} invalidInputs={['startsAt', 'title']} isLoading />);
// });

it('loading matches snapshot with invalid inputs', () => {
  const tree = renderer
    .create(
      <EventForm
        onSubmit={() => null}
        invalidInputs={['startsAt', 'title']}
        isLoading
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})
