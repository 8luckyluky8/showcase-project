import * as React from 'react'
import { WrappedStyledComponent } from '../../theme'
import { withRouter, RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import StickyIcon from './StickyIcon'
import AppRoutes from '../../appRoutes'

interface StickyAddIconProps
  extends WrappedStyledComponent,
    RouteComponentProps {}

export const StickyAddIcon = ({ className }: StickyAddIconProps) => (
  <Link to={AppRoutes.AddEvent}>
    <StickyIcon className={className}>&#x2b;</StickyIcon>
  </Link>
)

export default withRouter(StickyAddIcon)
