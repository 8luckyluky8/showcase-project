import * as styledComponents from 'styled-components'
import * as React from 'react'
import { WrappedStyledComponent } from 'src/theme'

export interface StickyIconProps
  extends React.BaseHTMLAttributes<Element>,
    WrappedStyledComponent {
  children?: React.ReactNode
  onClick?: () => any
}

export interface StickyIconTheme {
  backgroundColor: string
  fontColor: string
  hoverColor?: string
}

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<
  StickyIconTheme
>

export { css, createGlobalStyle, keyframes, ThemeProvider }
export default styled
