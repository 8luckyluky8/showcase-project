import * as React from 'react'
import { WrappedStyledComponent } from 'src/theme'

interface ShowSecretIconProps extends WrappedStyledComponent {
  onClick?: () => any
}

const ShowSecretIcon = ({ onClick, className }: ShowSecretIconProps) => (
  <svg
    className={className}
    onClick={onClick}
    width="22px"
    height="16px"
    viewBox="0 0 22 16"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        id="00-1-2-Log-In"
        transform="translate(-1177.000000, -539.000000)"
        fill="#E1E4E6"
      >
        <g id="Pass" transform="translate(720.000000, 512.000000)">
          <g id="icon-show" transform="translate(456.000000, 23.000000)">
            <path
              d="M11.9999993,4.49999973 C6.99999958,4.49999973 2.72999985,7.60999967 0.99999994,11.9999993 C2.72999985,16.3899983 6.99999958,19.4999988 11.9999993,19.4999988 C16.999999,19.4999988 21.2699991,16.3899983 22.9999986,11.9999993 C21.2699991,7.60999967 16.999999,4.49999973 11.9999993,4.49999973 Z M11.9999993,16.999999 C9.23999922,16.999999 6.99999958,14.7599993 6.99999958,11.9999993 C6.99999958,9.23999922 9.23999922,6.99999958 11.9999993,6.99999958 C14.7599993,6.99999958 16.999999,9.23999922 16.999999,11.9999993 C16.999999,14.7599993 14.7599993,16.999999 11.9999993,16.999999 Z M11.9999993,8.99999946 C10.3399995,8.99999946 8.99999946,10.3399995 8.99999946,11.9999993 C8.99999946,13.659999 10.3399995,14.9999991 11.9999993,14.9999991 C13.659999,14.9999991 14.9999991,13.659999 14.9999991,11.9999993 C14.9999991,10.3399995 13.659999,8.99999946 11.9999993,8.99999946 Z"
              id="Fill-2"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
)

export default ShowSecretIcon
