import * as React from 'react'
import theme, { WrappedStyledComponent } from '../../theme'
import styled from 'styled-components'

interface ListViewIconProps extends WrappedStyledComponent {
  isActive?: boolean
  onClick?: () => any
}

const ListViewIcon = ({ className, onClick, isActive }: ListViewIconProps) => (
  <svg
    className={className}
    onClick={onClick}
    width="17px"
    height="13px"
    viewBox="0 0 17 13"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="01-1-1-Dashboard" transform="translate(-1303.000000, -174.000000)">
        <g id="Switcher" transform="translate(1267.000000, 169.000000)">
          <g id="list-view" transform="translate(32.000000, 0.000000)">
            <polygon
              id="Stroke-1"
              strokeOpacity="0.00784313771"
              stroke="#000000"
              strokeWidth="1.33333335e-11"
              points="0 0 23.9999985 0 23.9999985 23.9999985 0 23.9999985"
            />
            <path
              d="M3.99999976,17.9999989 L20.9999987,17.9999989 L20.9999987,11.9999993 L3.99999976,11.9999993 L3.99999976,17.9999989 Z M3.99999976,4.9999997 L3.99999976,10.9999993 L20.9999987,10.9999993 L20.9999987,4.9999997 L3.99999976,4.9999997 Z"
              id="Fill-2"
              fill={isActive ? theme.primaryColor : theme.ternaryColor}
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
)

export default styled(ListViewIcon)`
  cursor: pointer;
`
