import * as React from 'react'
import { shallow } from 'enzyme'
import ListViewIcon from '../ListViewIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<ListViewIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<ListViewIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})

it('active renders without crashing', () => {
  shallow(<ListViewIcon isActive />)
})

it('active matches snapshot', () => {
  const tree = renderer.create(<ListViewIcon isActive />).toJSON()

  expect(tree).toMatchSnapshot()
})
