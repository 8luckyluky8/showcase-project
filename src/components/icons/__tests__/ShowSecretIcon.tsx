import * as React from 'react'
import { shallow } from 'enzyme'
import ShowSecretIcon from '../ShowSecretIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<ShowSecretIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<ShowSecretIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})
