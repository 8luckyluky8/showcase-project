import * as React from 'react'
import { shallow } from 'enzyme'
import StormTrooperIcon from '../StormTrooperIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<StormTrooperIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<StormTrooperIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})
