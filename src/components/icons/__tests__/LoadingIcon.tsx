import * as React from 'react'
import { shallow } from 'enzyme'
import LoadingIcon from '../LoadingIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<LoadingIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<LoadingIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})
