import * as React from 'react'
import { shallow } from 'enzyme'
import CloseIcon from '../CloseIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<CloseIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<CloseIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})
