import * as React from 'react'
import { shallow } from 'enzyme'
import GridViewIcon from '../GridViewIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<GridViewIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<GridViewIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})
