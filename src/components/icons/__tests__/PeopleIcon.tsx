import * as React from 'react'
import { shallow } from 'enzyme'
import PeopleIcon from '../PeopleIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PeopleIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PeopleIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})
