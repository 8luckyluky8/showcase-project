import * as React from 'react'
import { shallow } from 'enzyme'
import StickyIcon from '../StickyIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<StickyIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<StickyIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})

const testTheme = {
  backgroundColor: '#fff',
  fontColor: '#000',
  hoverColor: '#aaa',
}

it('with theme renders without crashing', () => {
  shallow(<StickyIcon theme={testTheme} />)
})

it('with theme matches snapshot', () => {
  const tree = renderer.create(<StickyIcon theme={testTheme} />).toJSON()

  expect(tree).toMatchSnapshot()
})
