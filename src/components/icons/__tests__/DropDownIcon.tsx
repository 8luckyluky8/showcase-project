import * as React from 'react'
import { shallow } from 'enzyme'
import DropDownIcon from '../DropDownIcon'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<DropDownIcon />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<DropDownIcon />).toJSON()

  expect(tree).toMatchSnapshot()
})

it('blue renders without crashing', () => {
  shallow(<DropDownIcon color="blue" />)
})

it('blue matches snapshot', () => {
  const tree = renderer.create(<DropDownIcon color="blue" />).toJSON()

  expect(tree).toMatchSnapshot()
})
