import * as React from 'react'
import theme, { WrappedStyledComponent } from '../../theme'
import styled from './styled-sticky-icon'

interface StickyIconProps extends WrappedStyledComponent {
  children?: React.ReactNode
}

const StickyIcon = ({ className, children }: StickyIconProps) => (
  <div className={className}>{children}</div>
)

const StyledStickyIcon = styled(StickyIcon)`
  width: 56px;
  height: 56px;
  background-color: ${props => props.theme.backgroundColor};
  color: ${props => props.theme.fontColor};
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 28px;
  position: fixed;
  bottom: 32px;
  right: 32px;
  box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);

  &:hover {
    background-color: ${props => props.theme.hoverColor};
  }
`

StyledStickyIcon.defaultProps = {
  theme: {
    backgroundColor: theme.primaryColor,
    fontColor: theme.secondaryColor,
    hoverColor: theme.primaryHoverColor,
  },
}

export default StyledStickyIcon
