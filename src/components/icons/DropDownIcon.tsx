import * as React from 'react'
import theme, { WrappedStyledComponent } from '../../theme'

interface DropDownIconProps extends WrappedStyledComponent {
  color?: string
  onClick?: React.MouseEventHandler<SVGElement>
}

const DropDownIcon = ({ className, color, onClick }: DropDownIconProps) => (
  <svg
    onClick={onClick}
    className={className}
    width="10px"
    height="5px"
    viewBox="0 0 10 5"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g
      id="–-Styleguide"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g
        id="Styleguide"
        transform="translate(-243.000000, -2107.000000)"
        fill={color || theme.inactiveColor}
      >
        <g id="Dropdown" transform="translate(120.000000, 1980.000000)">
          <g id="Profile" transform="translate(0.000000, 109.000000)">
            <g id="dropdown" transform="translate(123.000000, 18.000000)">
              <polygon id="Fill-2" points="0 0 5 5 10 0" />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
)

export default DropDownIcon
