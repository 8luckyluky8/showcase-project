import styled from './styled-text'
import theme from '../../theme'

const Heading = styled.h1`
  color: ${theme.primaryFontColor};
  font-size: 1.75rem;
  font-family: Hind-Regular, sans-serif;
  text-align: center;
  margin: 0;
`

export default Heading
