import * as React from 'react'
import { User } from '../../models/user'
import PrimaryTextMedium from './PrimaryTextMedium'
import { WrappedStyledComponent } from '../../theme'

interface UserNameProps extends WrappedStyledComponent {
  user: User
}

const UserName = ({ className, user }: UserNameProps) => (
  <PrimaryTextMedium className={className}>{`${user.firstName} ${
    user.lastName
  }`}</PrimaryTextMedium>
)

export default UserName
