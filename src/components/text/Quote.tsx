import styled from './styled-text'
import theme from '../../theme'

const Quote = styled.span`
  font-family: PlayfairDisplay-Regular, sans-serif;
  font-size: 2.25rem;
  color: ${theme.secondaryColor};
  margin: 4px;
  text-align: center;
`

export default Quote
