import styled from './styled-text'
import theme from '../../theme'

const SecondaryText = styled.span`
  color: ${theme.ternaryFontColor};
  font-size: 0.9rem;
`

export default SecondaryText
