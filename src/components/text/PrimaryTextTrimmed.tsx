import styled from 'styled-components'
import PrimaryText from './PrimaryText'

const PrimaryTextTrimmed = styled(PrimaryText)`
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`

export default PrimaryTextTrimmed
