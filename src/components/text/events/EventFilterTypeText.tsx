import styled from 'styled-components'
import theme from '../../../theme'

interface EventFilterTypeTextProps {
  isActive?: boolean
}

const EventFilterTypeText = styled.span`
  font-family: Hind-Semibold, sans-serif;
  font-size: 0.75rem;
  color: ${(props: EventFilterTypeTextProps) =>
    props.isActive ? theme.primaryFontColor : theme.inactiveColor};
  cursor: pointer;
`

export default EventFilterTypeText
