import * as React from 'react'
import styled from 'styled-components'
import { WrappedStyledComponent } from '../../../theme'
import { User } from '../../../models/user'

interface EventOwnerProps extends WrappedStyledComponent {
  owner: User
}

const EventOwner = ({ className, owner }: EventOwnerProps) => (
  <span className={className}>{`${owner.firstName} ${owner.lastName}`}</span>
)

export default styled(EventOwner)`
  font-family: Hind-Regular, sans-serif;
  color: #7d7878;
  font-size: 0.875rem;
`
