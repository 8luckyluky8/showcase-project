import styled from 'styled-components'
import Heading from '../Heading'

const EventTitle = styled(Heading).attrs({
  as: 'h2',
})`
  font-size: 1.375rem;
  text-align: left;
  margin-bottom: 0;
`

export default EventTitle
