import * as React from 'react'
import { shallow } from 'enzyme'
import EventTitle from '../EventTitle'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<EventTitle />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<EventTitle />).toJSON()

  expect(tree).toMatchSnapshot()
})
