import * as React from 'react'
import { shallow } from 'enzyme'
import EventFilterTypeText from '../EventFilterTypeText'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<EventFilterTypeText />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<EventFilterTypeText />).toJSON()

  expect(tree).toMatchSnapshot()
})

it('active renders without crashing', () => {
  shallow(<EventFilterTypeText isActive />)
})

it('active matches snapshot', () => {
  const tree = renderer.create(<EventFilterTypeText isActive />).toJSON()

  expect(tree).toMatchSnapshot()
})
