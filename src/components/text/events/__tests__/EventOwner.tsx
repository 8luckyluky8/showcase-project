import * as React from 'react'
import { shallow } from 'enzyme'
import EventOwner from '../EventOwner'
import * as renderer from 'react-test-renderer'
import { testUser } from '../../../../setupTests'

it('renders without crashing', () => {
  shallow(<EventOwner owner={testUser} />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<EventOwner owner={testUser} />).toJSON()

  expect(tree).toMatchSnapshot()
})
