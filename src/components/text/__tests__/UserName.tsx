import * as React from 'react'
import { shallow } from 'enzyme'
import UserName from '../UserName'
import * as renderer from 'react-test-renderer'
import { testUser } from '../../../setupTests'

it('renders without crashing', () => {
  shallow(<UserName user={testUser} />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<UserName user={testUser} />).toJSON()

  expect(tree).toMatchSnapshot()
})
