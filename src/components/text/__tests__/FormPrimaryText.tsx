import * as React from 'react'
import { shallow } from 'enzyme'
import FormPrimaryText from '../FormPrimaryText'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<FormPrimaryText />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<FormPrimaryText />).toJSON()

  expect(tree).toMatchSnapshot()
})
