import * as React from 'react'
import { shallow } from 'enzyme'
import PrimaryText from '../PrimaryText'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PrimaryText />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PrimaryText />).toJSON()

  expect(tree).toMatchSnapshot()
})
