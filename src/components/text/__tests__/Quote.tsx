import * as React from 'react'
import { shallow } from 'enzyme'
import Quote from '../Quote'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<Quote />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<Quote />).toJSON()

  expect(tree).toMatchSnapshot()
})
