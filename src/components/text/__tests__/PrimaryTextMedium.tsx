import * as React from 'react'
import { shallow } from 'enzyme'
import PrimaryTextMedium from '../PrimaryTextMedium'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PrimaryTextMedium />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PrimaryTextMedium />).toJSON()

  expect(tree).toMatchSnapshot()
})
