import * as React from 'react'
import { shallow } from 'enzyme'
import QuoteAuthor from '../QuoteAuthor'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<QuoteAuthor />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<QuoteAuthor />).toJSON()

  expect(tree).toMatchSnapshot()
})
