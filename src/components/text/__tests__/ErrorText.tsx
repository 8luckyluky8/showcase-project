import * as React from 'react'
import { shallow } from 'enzyme'
import ErrorText from '../ErrorText'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<ErrorText />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<ErrorText />).toJSON()

  expect(tree).toMatchSnapshot()
})
