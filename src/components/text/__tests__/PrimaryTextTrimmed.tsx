import * as React from 'react'
import { shallow } from 'enzyme'
import PrimaryTextTrimmed from '../PrimaryTextTrimmed'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PrimaryTextTrimmed />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PrimaryTextTrimmed />).toJSON()

  expect(tree).toMatchSnapshot()
})
