import * as React from 'react'
import { shallow } from 'enzyme'
import SecondaryText from '../SecondaryText'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<SecondaryText />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<SecondaryText />).toJSON()

  expect(tree).toMatchSnapshot()
})
