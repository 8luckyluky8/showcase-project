import * as React from 'react'
import { shallow } from 'enzyme'
import Heading from '../Heading'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<Heading />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<Heading />).toJSON()

  expect(tree).toMatchSnapshot()
})
