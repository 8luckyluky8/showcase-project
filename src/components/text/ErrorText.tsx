import styled from './styled-text'
import theme from '../../theme'

const ErrorText = styled.span`
  color: ${theme.errorColor};
  font-family: Hind-Regular, sans-serif;
  font-size: 1.125rem;
  text-align: center;
`

export default ErrorText
