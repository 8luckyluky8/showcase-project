import styled from './styled-text'
import PrimaryText from './PrimaryText'

const FormPrimaryText = styled(PrimaryText)`
  margin-bottom: 0.125rem;
`

export default FormPrimaryText
