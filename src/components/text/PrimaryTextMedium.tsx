import styled from './styled-text'
import theme from '../../theme'

const PrimaryTextMedium = styled.span`
  color: ${theme.secondaryFontColor};
  font-family: Hind-Medium, sans-serif;
  font-size: 0.875rem;
`

export default PrimaryTextMedium
