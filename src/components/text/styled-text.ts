import * as styledComponents from 'styled-components'
import { ReactNode } from 'react'

export interface TextProps {
  children?: ReactNode
}

export interface TextTheme {
  color: string
  fontColor: string
}

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<TextTheme>

export { css, createGlobalStyle, keyframes, ThemeProvider }
export default styled
