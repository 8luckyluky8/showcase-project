import styled from './styled-text'
import theme from '../../theme'

const QuoteAuthor = styled.span`
  font-family: Hind-Regular, sans-serif;
  font-size: 1.125rem;
  color: ${theme.secondaryFontColor};
  text-align: center;
`

export default QuoteAuthor
