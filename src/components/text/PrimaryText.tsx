import styled from './styled-text'
import theme from '../../theme'

const PrimaryText = styled.span`
  color: ${theme.secondaryFontColor};
  font-size: 1rem;
  font-family: Hind-Regular, sans-serif;
`

export default PrimaryText
