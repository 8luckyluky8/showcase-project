import styled from 'styled-components'
import { media } from '../../theme'

const PrivateMenuContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  margin-right: 35px;
  ${/* sc-declaration */ media.desktop`
    margin-right: 0;
  `}

  & > * {
    margin-left: 8px;
  }
`

export default PrivateMenuContainer
