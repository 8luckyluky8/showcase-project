import * as React from 'react'
import PrivateMenuContainer from './PrivateMenuContainer'
import CloseIcon from '../icons/CloseIcon'
import { withRouter, RouteComponentProps } from 'react-router'
import styled from 'styled-components'
import theme from '../../theme'

const CloseText = styled.span`
  font-family: Hind-Regular, sans-serif;
  font-size: 1rem;
  color: ${theme.primaryFontColor};
  padding-top: 1px;
`

interface CloseMenuProps extends RouteComponentProps {}

export const CloseMenu = ({ history }: CloseMenuProps) => {
  const onClick = () => {
    history.goBack()
  }

  return (
    <PrivateMenuContainer onClick={onClick}>
      <CloseIcon />

      <CloseText>Close</CloseText>
    </PrivateMenuContainer>
  )
}

export default withRouter(CloseMenu)
