import * as React from 'react'
import DropDownMenu from '../DropDownMenu'
import ItemsList from '../ItemsList'
import PrimaryTextMedium from '../../text/PrimaryTextMedium'
import { withRouter, RouteComponentProps } from 'react-router'
import { connect } from 'react-redux'
import { Dispatch } from '../../../store'
import styled from 'styled-components'
import AppRoutes from '../../../appRoutes'

/**
 *
 * UI components
 *
 */

const StyledDropDownMenu = styled(DropDownMenu)`
  width: 162px;
  cursor: default;
`

const MenuItem = styled(PrimaryTextMedium).attrs({
  as: 'a',
})`
  cursor: pointer;
`

/**
 *
 * Redux connect mappings
 *
 */

const mapDispatch = (dispatch: Dispatch) => ({
  clearUser: () => dispatch.user.clearUser(),
})

/**
 *
 * Renders dropdown menu with user actions.
 *
 */

interface UserDropDownMenuProps extends RouteComponentProps {
  isVisible?: boolean
}

export class UserDropDownMenu extends React.Component<
  UserDropDownMenuProps & ReturnType<typeof mapDispatch>,
  {}
> {
  onLogout = () => {
    this.props.clearUser()
    this.props.history.push(AppRoutes.SignIn)
  }

  render() {
    return (
      <React.Fragment>
        <StyledDropDownMenu isVisible={this.props.isVisible}>
          <ItemsList
            items={[
              // @TODO: Handle profile click and redirect to profile page
              {
                item: <MenuItem>Profile</MenuItem>,
                key: 'profile',
              },
              {
                item: <MenuItem onClick={this.onLogout}>Logout</MenuItem>,
                key: 'logout',
              },
            ]}
          />
        </StyledDropDownMenu>
      </React.Fragment>
    )
  }
}

export default withRouter(
  connect(
    null,
    mapDispatch
  )(UserDropDownMenu)
)
