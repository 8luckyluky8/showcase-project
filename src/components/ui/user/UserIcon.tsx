import * as React from 'react'
import { User } from 'src/models/user'
import styled from 'styled-components'
import theme, { WrappedStyledComponent } from '../../../theme'

interface UserIconProps extends WrappedStyledComponent {
  user: User
}

const getUserDefaultText = (user: User): string =>
  `${user.firstName.charAt(0).toUpperCase()} ${user.firstName
    .charAt(1)
    .toUpperCase()}`

const UserIcon = ({ className, user }: UserIconProps) => (
  <div className={className}>{getUserDefaultText(user)}</div>
)

export default styled(UserIcon)`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${theme.ternaryColor};
  width: 40px;
  height: 40px;
  border-radius: 50%;
  color: ${theme.secondaryFontColor};
  font-family: Hind-Medium, sans-serif;
  font-size: 0.875rem;
  letter-spacing: 0;
`
