import * as React from 'react'
import { shallow } from 'enzyme'
import UserIcon from '../UserIcon'
import * as renderer from 'react-test-renderer'
import { testUser } from '../../../../setupTests'

it('renders without crashing', () => {
  shallow(<UserIcon user={testUser} />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<UserIcon user={testUser} />).toJSON()

  expect(tree).toMatchSnapshot()
})
