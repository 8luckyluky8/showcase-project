import * as React from 'react'
import { shallow } from 'enzyme'
import { UserDropDownMenu } from '../UserDropDownMenu'
import * as renderer from 'react-test-renderer'
import { history, location, match } from '../../../../setupTests'

it('renders without crashing', () => {
  shallow(
    <UserDropDownMenu
      history={history}
      location={location}
      match={match}
      clearUser={() => {
        return { payload: null, type: '' }
      }}
    />
  )
})

it('matches snapshot', () => {
  const tree = renderer
    .create(
      <UserDropDownMenu
        history={history}
        location={location}
        match={match}
        clearUser={() => {
          return { payload: null, type: '' }
        }}
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})
