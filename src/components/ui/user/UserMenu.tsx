import * as React from 'react'
import UserIcon from './UserIcon'
import styled from 'styled-components'
import UserName from '../../text/UserName'
import { media } from '../../../theme'
import { User } from '../../../models/user'
import DropDownIcon from '../../icons/DropDownIcon'
import UserDropDownMenu from './UserDropDownMenu'
import PrivateMenuContainer from '../PrivateMenuContainer'

/**
 *
 * UI components
 *
 */

const MenuUserName = styled(UserName)`
  display: none;
  ${/* sc-declaration */ media.desktop`
    display: inline-block;
  `}
`

/**
 *
 * Renders user menu with avatar and dropdown menu with user actions.
 *
 */

interface UserMenuProps {
  user: User
}

const UserMenu = ({ user }: UserMenuProps) => {
  const [isDropdownVisible, setIsDropdownVisible] = React.useState(false)

  const toggleDropdownVisibility = () => {
    setIsDropdownVisible(!isDropdownVisible)
  }

  return (
    <React.Fragment>
      <PrivateMenuContainer onClick={toggleDropdownVisibility}>
        <UserIcon user={user} />

        <MenuUserName user={user} />

        <DropDownIcon />
      </PrivateMenuContainer>

      <UserDropDownMenu isVisible={isDropdownVisible} />
    </React.Fragment>
  )
}

export default UserMenu
