import * as React from 'react'
import styled from 'styled-components'
import SignUpPrompt from './SignUpPrompt'
import { media } from '../../theme'
import Logo from '../images/Logo'

const HeaderContainer = styled.div`
  margin: 0;
  ${/* sc-declaration */ media.desktop`
    margin: 40px;
  `}
`

const MobileLogo = styled(Logo)`
  display: block;
  ${/* sc-declaration */ media.desktop`
    display: none;
  `}
`

const DesktopSignUpPrompt = styled(SignUpPrompt)`
  display: none;
  ${/* sc-declaration */ media.desktop`
    display: block;
  `}
`

const PublicHeader = () => (
  <HeaderContainer>
    <MobileLogo />

    <DesktopSignUpPrompt />
  </HeaderContainer>
)

export default PublicHeader
