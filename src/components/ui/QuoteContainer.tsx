import * as React from 'react'
import styled from 'styled-components'
import Quote from '../text/Quote'
import QuoteSeparator from '../images/QuoteSeparator'
import QuoteAuthor from '../text/QuoteAuthor'
import { WrappedStyledComponent } from 'src/theme'

interface QuoteContainerProps extends WrappedStyledComponent {
  quoteAuthor?: string
  quote: string
}

const QuoteContainer = ({
  className,
  quoteAuthor,
  quote,
}: QuoteContainerProps) => (
  <div className={className}>
    <Quote>{quote}</Quote>

    {quoteAuthor && (
      <React.Fragment>
        <QuoteSeparator />

        <QuoteAuthor>{quoteAuthor}</QuoteAuthor>
      </React.Fragment>
    )}
  </div>
)

export default styled(QuoteContainer)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: 'center';
  padding: 4px;
  margin-bottom: 82px;
`
