import * as React from 'react'
import styled from 'styled-components'
import theme, { WrappedStyledComponent } from '../../theme'

/**
 *
 * UI components
 *
 */

const Triangle = styled.div`
  width: 58px;
  height: 17px;
  top: -4px;
  position: absolute;
  overflow: hidden;
  box-shadow: 0 0 10px -17px rgba(0, 0, 0, 0.2);

  &::after {
    content: '';
    position: absolute;
    width: 20px;
    height: 20px;
    background: ${theme.secondaryColor};
    transform: rotate(45deg);
    top: 7px;
    left: 20px;
    box-shadow: -1px -1px 10px -2px rgba(0, 0, 0, 0.2);
  }
`

const DropDownMenu = styled.div`
  border-radius: 14px;
  background-color: ${theme.secondaryColor};
  box-shadow: 0 5px 15px 0 rgba(0, 0, 0, 0.2);
  padding: 16px;
  margin-top: 12px;
  width: 162px;
`

const DropDownContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-end;
  z-index: 1000;
`

/**
 *
 * Component for rendering passed children in dropdown menu.
 *
 */

interface DropDownMenuContainerProps extends WrappedStyledComponent {
  children?: React.ReactNode
  isVisible?: boolean
}

const DropDownMenuContainer = ({
  className,
  children,
  isVisible,
}: DropDownMenuContainerProps) => {
  if (!isVisible) {
    return null
  }

  return (
    <div className={className}>
      <DropDownContainer>
        <Triangle />

        <DropDownMenu>{children}</DropDownMenu>
      </DropDownContainer>
    </div>
  )
}

export default styled(DropDownMenuContainer)`
  position: absolute;
  top: 80px;
  right: 20px;
`
