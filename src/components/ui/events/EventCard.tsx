import * as React from 'react'
import Card, { listViewColumnGap } from '../Card'
import { EventData } from 'src/models/events'
import { WrappedStyledComponent, media } from 'src/theme'
import styled from 'styled-components'
import { formatDate } from 'src/tools/date'
import SecondaryText from 'src/components/text/SecondaryText'
import EventTitle from 'src/components/text/events/EventTitle'
import EventOwner from 'src/components/text/events/EventOwner'
import EventAttendees from './EventAttendees'
import { ViewFilterType } from '../ViewFilter'
import PrimaryTextTrimmed from 'src/components/text/PrimaryTextTrimmed'
import PrimaryText from 'src/components/text/PrimaryText'
import PositiveButton from 'src/components/buttons/PositiveButton'
import NegativeButton from 'src/components/buttons/NegativeButton'
import NeutralButton from 'src/components/buttons/NeutralButton'

/**
 *
 * Render event card based on view type (ViewFilterType)
 *
 */

interface EventCardProps extends WrappedStyledComponent {
  event: EventData
  viewType?: ViewFilterType
  isUserInEvent: boolean
  isUserOwner: boolean
  onAttendEvent: () => any
  onUnattendEvent: () => any
  onEditEvent: () => any
}

const CardGridUserActionsRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
  width: 100%;
  justify-self: flex-end;
`

const EventCardGridView = ({
  className,
  event,
  isUserInEvent,
  isUserOwner,
  onAttendEvent,
  onUnattendEvent,
  onEditEvent,
}: EventCardProps) => (
  <Card className={className} viewType={ViewFilterType.GridView}>
    <SecondaryText>{formatDate(event.startsAt)}</SecondaryText>

    <div>
      <EventTitle>{event.title}</EventTitle>

      <EventOwner owner={event.owner} />
    </div>

    <PrimaryText>{event.description}</PrimaryText>

    <CardGridUserActionsRow>
      <EventAttendees event={event} />

      {isUserOwner ? (
        <NeutralButton onClick={onEditEvent}>EDIT</NeutralButton>
      ) : isUserInEvent ? (
        <NegativeButton onClick={onUnattendEvent}>LEAVE</NegativeButton>
      ) : (
        <PositiveButton onClick={onAttendEvent}>JOIN</PositiveButton>
      )}
    </CardGridUserActionsRow>
  </Card>
)

const CardListEventTitle = styled(EventTitle)`
  font-size: 1.125rem;
  margin-top: 0;
`

const CardListFooter = styled.div`
  display: grid;
  grid-column-gap: 0;
  align-items: center;
  justify-items: flex-start;
  grid-auto-flow: column;
  grid-template-rows: repeat(2, 1fr);
  grid-template-columns: repeat(2, 1fr);
  margin-top: 8px;
  ${/* sc-declaration */ media.desktop`
    grid-column-gap: ${listViewColumnGap};
    grid-auto-flow: row;
    grid-template-rows: 1fr;
    grid-template-columns: repeat(3, 1fr);
    justify-items: center;
    margin-top: 0;
  `}

  > button {
    grid-row: span 2;
    justify-self: flex-end;
    ${/* sc-declaration */ media.desktop`
      justify-self: center;
    `}
  }
`

const CardListEventOwner = styled(EventOwner)`
  display: none;
  ${/* sc-declaration */ media.desktop`
    display: block;
  `}
`

const EventCardListView = ({
  className,
  event,
  isUserInEvent,
  isUserOwner,
  onAttendEvent,
  onUnattendEvent,
  onEditEvent,
}: EventCardProps) => (
  <Card className={className} viewType={ViewFilterType.ListView}>
    <CardListEventTitle>{event.title}</CardListEventTitle>

    <PrimaryTextTrimmed>{event.description}</PrimaryTextTrimmed>

    <CardListEventOwner owner={event.owner} />

    <CardListFooter>
      <SecondaryText>{formatDate(event.startsAt)}</SecondaryText>

      <EventAttendees event={event} hideIcon />

      {isUserOwner ? (
        <NeutralButton onClick={onEditEvent}>EDIT</NeutralButton>
      ) : isUserInEvent ? (
        <NegativeButton onClick={onUnattendEvent}>LEAVE</NegativeButton>
      ) : (
        <PositiveButton onClick={onAttendEvent}>JOIN</PositiveButton>
      )}
    </CardListFooter>
  </Card>
)

const EventCard = ({ viewType, ...rest }: EventCardProps) => {
  if (viewType === ViewFilterType.ListView) {
    return <EventCardListView {...rest} />
  }

  return <EventCardGridView {...rest} />
}

export default styled(EventCard)`
  margin: 16px 0;
`
