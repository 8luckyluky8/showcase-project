import * as React from 'react'
import { EventData } from 'src/models/events'
import { WrappedStyledComponent, media } from 'src/theme'
import styled from 'styled-components'
import EventCard from './EventCard'
import { ViewFilterType } from '../ViewFilter'
import { User } from 'src/models/user'

/**
 *
 * Renders events list.
 *
 */

interface EventListProps extends WrappedStyledComponent {
  events: EventData[]
  viewFilterType: ViewFilterType
  user: User
  onAttendEvent: (eventId: EventData['id']) => any
  onUnattendEvent: (eventId: EventData['id']) => any
  onEditEvent: (eventId: EventData['id']) => any
}

const EventList = ({
  className,
  events,
  viewFilterType,
  user,
  onAttendEvent,
  onUnattendEvent,
  onEditEvent,
}: EventListProps) => (
  <div className={className}>
    {events.map((event: EventData) => (
      <EventCard
        key={event.id}
        event={event}
        viewType={viewFilterType}
        isUserInEvent={event.attendees.some(
          (attendee: User) => attendee.id === user.id
        )}
        isUserOwner={event.owner.id === user.id}
        onAttendEvent={() => onAttendEvent(event.id)}
        onUnattendEvent={() => onUnattendEvent(event.id)}
        onEditEvent={() => onEditEvent(event.id)}
      />
    ))}
  </div>
)

export default styled(EventList)`
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 15px;
  width: 100%;
  ${/* sc-declaration */ media.desktop`
    grid-template-columns: ${(props: EventListProps) =>
      props.viewFilterType === ViewFilterType.GridView ? '1fr 1fr' : '1fr'};
    /* grid-template-columns: 1fr 1fr; */
  `}
  ${/* sc-declaration */ media.largeDesktop`
    grid-template-columns: ${(props: EventListProps) =>
      props.viewFilterType === ViewFilterType.GridView ? '1fr 1fr 1fr' : '1fr'};
    /* grid-template-columns: 1fr 1fr 1fr; */
  `}
`
