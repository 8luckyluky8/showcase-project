import * as React from 'react'
import { WrappedStyledComponent } from 'src/theme'
import { EventData } from 'src/models/events'
import styled from 'styled-components'
import PeopleIcon from 'src/components/icons/PeopleIcon'
import PrimaryText from 'src/components/text/PrimaryText'

interface AttendeesTextProps {
  hideIcon?: boolean
}

const AttendeesText = styled(PrimaryText)`
  font-size: 0.875rem;
  margin-left: ${({ hideIcon }: AttendeesTextProps) =>
    hideIcon ? '0' : '7px'};
`

interface EventAttendeesProps extends WrappedStyledComponent {
  event: EventData
  hideIcon?: boolean
}

const EventAttendees = ({
  className,
  event,
  hideIcon,
}: EventAttendeesProps) => (
  <div className={className}>
    {!hideIcon && <PeopleIcon />}

    <AttendeesText hideIcon={hideIcon}>
      {`${event.attendees.length} of ${event.capacity}`}
    </AttendeesText>
  </div>
)

export default styled(EventAttendees)`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`
