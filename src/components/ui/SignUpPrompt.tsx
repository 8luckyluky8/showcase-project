import * as React from 'react'
import PrimaryTextMedium from '../text/PrimaryTextMedium'
import SecondaryText from '../text/SecondaryText'
import { WrappedStyledComponent } from '../../theme'

interface SignUpPromptProps extends WrappedStyledComponent {}

const SignUpPrompt = ({ className }: SignUpPromptProps) => (
  <div className={className}>
    <SecondaryText>Don't have an account?</SecondaryText>{' '}
    {/* @TODO: Add screen for route */}
    <PrimaryTextMedium>SIGN UP</PrimaryTextMedium>
  </div>
)

export default SignUpPrompt
