import * as React from 'react'
import { WrappedStyledComponent } from 'src/theme'
import styled from 'styled-components'
import GridViewIcon from '../icons/GridViewIcon'
import ListViewIcon from '../icons/ListViewIcon'

export enum ViewFilterType {
  GridView,
  ListView,
}

interface ViewFilterProps extends WrappedStyledComponent {
  activeFilter: ViewFilterType
  onFilterChange: (filterType: ViewFilterType) => any
}

const ViewFilter = ({
  className,
  activeFilter,
  onFilterChange,
}: ViewFilterProps) => (
  <div className={className}>
    <GridViewIcon
      isActive={activeFilter === ViewFilterType.GridView}
      onClick={() => onFilterChange(ViewFilterType.GridView)}
    />

    <ListViewIcon
      isActive={activeFilter === ViewFilterType.ListView}
      onClick={() => onFilterChange(ViewFilterType.ListView)}
    />
  </div>
)

export default styled(ViewFilter)`
  display: flex;
  flex-direction: row;
  > * {
    margin-left: 15px;
  }
`
