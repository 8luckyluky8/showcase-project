import * as React from 'react'
import ErrorPageScreen from '../../containers/ErrorPageScreen'

interface ErrorBoundaryProps {
  children?: React.ReactNode
  show?: boolean
}
interface ErrorBoundaryState {
  hasError: boolean
}

export default class ErrorBoundary extends React.Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  state: ErrorBoundaryState = {
    hasError: false,
  }

  static getDerivedStateFromError() {
    return { hasError: true }
  }

  render() {
    if (this.props.show || this.state.hasError) {
      return <ErrorPageScreen />
    }

    return this.props.children
  }
}
