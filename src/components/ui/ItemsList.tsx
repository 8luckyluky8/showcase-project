import * as React from 'react'
import styled from 'styled-components'
import { WrappedStyledComponent } from 'src/theme'

export interface ItemsListOption {
  key: string
  item: string | React.ReactNode
}

interface ItemsListProps extends WrappedStyledComponent {
  items: ItemsListOption[]
}

const ItemsList = ({ className, items }: ItemsListProps) => (
  <ul className={className}>
    {items.map(({ key, item }: ItemsListOption) => (
      <li key={key}>{item}</li>
    ))}
  </ul>
)

export default styled(ItemsList)`
  list-style-type: none;
  margin: 0;
  padding: 0;
`
