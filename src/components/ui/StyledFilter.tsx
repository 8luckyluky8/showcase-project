import * as React from 'react'
import styled from 'styled-components'
import { WrappedStyledComponent, media } from '../../theme'
import EventFilterTypeText from '../text/events/EventFilterTypeText'
import StyledSelect from '../inputs/StyledSelect'

/**
 *
 * Renders responsive filter (different layout for desktop and mobile)
 *
 */

export interface FilterOption {
  value: string
  label: string
}

interface FilterProps extends WrappedStyledComponent {
  onFilterSelect: (filterValue: string) => any
  selectedFilterValue: string
  filterOptions: FilterOption[]
}

export const FilterDesktop = ({
  className,
  selectedFilterValue,
  filterOptions,
  onFilterSelect,
}: FilterProps) => (
  <div className={className}>
    {filterOptions.map((filterOption: FilterOption) => (
      <EventFilterTypeText
        key={filterOption.value}
        isActive={selectedFilterValue === filterOption.value}
        onClick={() => onFilterSelect(filterOption.value)}
      >
        {filterOption.label}
      </EventFilterTypeText>
    ))}
  </div>
)
const StyledFilterDesktop = styled(FilterDesktop)`
  display: none;
  ${/* sc-declaration */ media.desktop`
    display: block;
    > * {
      margin-right: 32px;
    }
  `}
`

export const FilterMobile = ({
  className,
  selectedFilterValue,
  filterOptions,
  onFilterSelect,
}: FilterProps) => {
  const selectedValue = filterOptions.find(
    (filterOption: FilterOption) => selectedFilterValue === filterOption.value
  )

  return (
    <div className={className}>
      <EventFilterTypeText isActive={false}>SHOW: </EventFilterTypeText>

      <StyledSelect
        value={selectedValue ? selectedValue.value : ''}
        onChange={event => onFilterSelect(event.target.value)}
      >
        {filterOptions.map((filterOption: FilterOption) => (
          <option key={filterOption.value} value={filterOption.value}>
            {filterOption.label}
          </option>
        ))}
      </StyledSelect>
    </div>
  )
}

const StyledFilterMobile = styled(FilterMobile)`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  ${/* sc-declaration */ media.desktop`
    display: none;
  `}
`

const Filter = ({ className, ...rest }: FilterProps) => (
  <div className={className}>
    <StyledFilterDesktop {...rest} />

    <StyledFilterMobile {...rest} />
  </div>
)

const StyledFilter = styled(Filter)`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`

export default StyledFilter
