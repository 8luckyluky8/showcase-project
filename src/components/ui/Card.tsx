import styled, { css } from 'styled-components'
import theme, { media } from '../../theme'
import { ViewFilterType } from './ViewFilter'

/**
 *
 * Handles different view types.
 * Defaults to layout for ViewFilterType.GridView without restricted width
 *
 */

interface CardProps {
  viewType?: ViewFilterType
}

const gridViewStyles = css`
  display: grid;
  grid-row-gap: 32px;
`

export const listViewColumnGap = '40px'

const listViewStyles = css`
  display: grid;
  grid-template-columns: 1fr;
  align-items: flex-start;
  grid-column-gap: ${listViewColumnGap};
  ${/* sc-declaration */ media.desktop`
    grid-template-columns: 2fr 2fr 1fr 5fr;
    align-items: center;
  `}
`

const Card = styled.div<CardProps>`
  ${
    /* sc-declaration */ ({ viewType }: CardProps) =>
      viewType === ViewFilterType.ListView ? listViewStyles : gridViewStyles
  }
  background-color: ${theme.secondaryColor};
  border-radius: 2px;
  padding: 32px;
  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.2);
  max-width: 100%;
  ${
    /* sc-declaration */ media.desktop`
    max-width: ${({ viewType }: CardProps) =>
      viewType === ViewFilterType.GridView ? '326px' : '100%'};
  `
  }
`

export default Card
