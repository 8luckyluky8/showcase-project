import * as React from 'react'
import { shallow } from 'enzyme'
import QuoteContainer from '../QuoteContainer'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<QuoteContainer quote="Do, or do not. There is no try." />)
})

it('matches snapshot', () => {
  const tree = renderer
    .create(<QuoteContainer quote="Do, or do not. There is no try." />)
    .toJSON()

  expect(tree).toMatchSnapshot()
})

it('with author renders without crashing', () => {
  shallow(
    <QuoteContainer
      quote="Do, or do not. There is no try."
      quoteAuthor="Master Yoda"
    />
  )
})

it('with author matches snapshot', () => {
  const tree = renderer
    .create(
      <QuoteContainer
        quote="Do, or do not. There is no try."
        quoteAuthor="Master Yoda"
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})
