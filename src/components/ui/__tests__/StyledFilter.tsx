import * as React from 'react'
import { shallow } from 'enzyme'
import StyledFilter, { FilterDesktop, FilterMobile } from '../StyledFilter'
import * as renderer from 'react-test-renderer'

const testOptions = [
  {
    value: 'one',
    label: 'One',
  },
  {
    value: 'two',
    label: 'Two',
  },
]

it('renders without crashing', () => {
  shallow(
    <StyledFilter
      onFilterSelect={() => null}
      selectedFilterValue="one"
      filterOptions={testOptions}
    />
  )
})

it('matches snapshot', () => {
  const tree = renderer
    .create(
      <StyledFilter
        onFilterSelect={() => null}
        selectedFilterValue="one"
        filterOptions={testOptions}
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})

it('desktop renders without crashing', () => {
  shallow(
    <FilterDesktop
      onFilterSelect={() => null}
      selectedFilterValue="two"
      filterOptions={testOptions}
    />
  )
})

it('desktop matches snapshot', () => {
  const tree = renderer
    .create(
      <FilterDesktop
        onFilterSelect={() => null}
        selectedFilterValue="two"
        filterOptions={testOptions}
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})

it('mobile renders without crashing', () => {
  shallow(
    <FilterMobile
      onFilterSelect={() => null}
      selectedFilterValue="one"
      filterOptions={testOptions}
    />
  )
})

it('mobile matches snapshot', () => {
  const tree = renderer
    .create(
      <FilterMobile
        onFilterSelect={() => null}
        selectedFilterValue="one"
        filterOptions={testOptions}
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})
