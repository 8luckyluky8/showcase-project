import * as React from 'react'
import { shallow } from 'enzyme'
import SignUpPrompt from '../SignUpPrompt'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<SignUpPrompt />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<SignUpPrompt />).toJSON()

  expect(tree).toMatchSnapshot()
})
