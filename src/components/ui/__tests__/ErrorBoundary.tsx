import * as React from 'react'
import { shallow } from 'enzyme'
import ErrorBoundary from '../ErrorBoundary'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(
    <ErrorBoundary>
      <span>TEST</span>
    </ErrorBoundary>
  )
})

it('matches snapshot', () => {
  const tree = renderer
    .create(
      <ErrorBoundary>
        <span>TEST</span>
      </ErrorBoundary>
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})

const ErrorComponents = () => {
  throw new Error()
}

it('renders without crashing', () => {
  shallow(
    <ErrorBoundary>
      <ErrorComponents />
    </ErrorBoundary>
  )
})

it('matches snapshot', () => {
  const tree = renderer
    .create(
      <ErrorBoundary>
        <ErrorComponents />
      </ErrorBoundary>
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})
