import * as React from 'react'
import { shallow } from 'enzyme'
import ViewFilter, { ViewFilterType } from '../ViewFilter'
import * as renderer from 'react-test-renderer'

it('Grid view renders without crashing', () => {
  shallow(
    <ViewFilter
      onFilterChange={() => null}
      activeFilter={ViewFilterType.GridView}
    />
  )
})

it('Grid view matches snapshot', () => {
  const tree = renderer
    .create(
      <ViewFilter
        onFilterChange={() => null}
        activeFilter={ViewFilterType.GridView}
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})

it('List view renders without crashing', () => {
  shallow(
    <ViewFilter
      onFilterChange={() => null}
      activeFilter={ViewFilterType.ListView}
    />
  )
})

it('List view matches snapshot', () => {
  const tree = renderer
    .create(
      <ViewFilter
        onFilterChange={() => null}
        activeFilter={ViewFilterType.ListView}
      />
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})
