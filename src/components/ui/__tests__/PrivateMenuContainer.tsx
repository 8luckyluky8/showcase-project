import * as React from 'react'
import { shallow } from 'enzyme'
import PrivateMenuContainer from '../PrivateMenuContainer'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PrivateMenuContainer />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PrivateMenuContainer />).toJSON()

  expect(tree).toMatchSnapshot()
})
