import * as React from 'react'
import { shallow } from 'enzyme'
import Card from '../Card'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<Card />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<Card />).toJSON()

  expect(tree).toMatchSnapshot()
})
