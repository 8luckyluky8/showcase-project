import * as React from 'react'
import { shallow } from 'enzyme'
import PublicHeader from '../PublicHeader'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PublicHeader />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PublicHeader />).toJSON()

  expect(tree).toMatchSnapshot()
})
