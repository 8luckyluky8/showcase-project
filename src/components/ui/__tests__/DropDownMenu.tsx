import * as React from 'react'
import { shallow } from 'enzyme'
import DropDownMenu from '../DropDownMenu'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<DropDownMenu />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<DropDownMenu />).toJSON()

  expect(tree).toMatchSnapshot()
})

it('visible renders without crashing', () => {
  shallow(<DropDownMenu isVisible />)
})

it('visible matches snapshot', () => {
  const tree = renderer.create(<DropDownMenu isVisible />).toJSON()

  expect(tree).toMatchSnapshot()
})
