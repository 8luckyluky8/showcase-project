import * as React from 'react'
import { shallow } from 'enzyme'
import { CloseMenu } from '../CloseMenu'
import * as renderer from 'react-test-renderer'
import { history, location, match } from '../../../setupTests'

it('renders without crashing', () => {
  shallow(<CloseMenu history={history} location={location} match={match} />)
})

it('matches snapshot', () => {
  const tree = renderer
    .create(<CloseMenu history={history} location={location} match={match} />)
    .toJSON()

  expect(tree).toMatchSnapshot()
})
