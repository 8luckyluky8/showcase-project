import * as React from 'react'
import { shallow } from 'enzyme'
import ItemsList from '../ItemsList'
import * as renderer from 'react-test-renderer'

const testItemsString = [
  {
    key: 'foo',
    item: 'Foo',
  },
  {
    key: 'bar',
    item: 'Bar',
  },
]

it('string renders without crashing', () => {
  shallow(<ItemsList items={testItemsString} />)
})

it('string matches snapshot', () => {
  const tree = renderer.create(<ItemsList items={testItemsString} />).toJSON()

  expect(tree).toMatchSnapshot()
})

const testItemsNode = [
  {
    key: 'foo',
    item: <div>{'Foo'}</div>,
  },
  {
    key: 'bar',
    item: <div>{'Bar'}</div>,
  },
]

it('node renders without crashing', () => {
  shallow(<ItemsList items={testItemsNode} />)
})

it('node matches snapshot', () => {
  const tree = renderer.create(<ItemsList items={testItemsNode} />).toJSON()

  expect(tree).toMatchSnapshot()
})
