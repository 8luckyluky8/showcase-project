import * as React from 'react'
import { shallow } from 'enzyme'
import PrivateContent from '../PrivateContent'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PrivateContent />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PrivateContent />).toJSON()

  expect(tree).toMatchSnapshot()
})
