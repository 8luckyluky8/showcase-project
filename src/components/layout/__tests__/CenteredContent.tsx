import * as React from 'react'
import { shallow } from 'enzyme'
import CenteredContent from '../CenteredContent'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<CenteredContent />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<CenteredContent />).toJSON()

  expect(tree).toMatchSnapshot()
})
