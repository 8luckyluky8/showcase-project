import * as React from 'react'
import { shallow } from 'enzyme'
import { StyledPrivateLayout } from '../PrivateLayout'
import * as renderer from 'react-test-renderer'
import { testUser } from '../../../setupTests'

jest.mock('../../icons/StickyAddIcon', () => {
  return {
    default: () => null,
  }
})

jest.mock('../../ui/user/UserDropDownMenu', () => {
  return {
    default: () => null,
  }
})

it('renders without crashing', () => {
  shallow(<StyledPrivateLayout user={testUser} />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<StyledPrivateLayout user={testUser} />).toJSON()

  expect(tree).toMatchSnapshot()
})
