import * as React from 'react'
import { shallow } from 'enzyme'
import PublicLayout from '../PublicLayout'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PublicLayout />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PublicLayout />).toJSON()

  expect(tree).toMatchSnapshot()
})
