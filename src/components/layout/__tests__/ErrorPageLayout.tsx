import * as React from 'react'
import { shallow } from 'enzyme'
import ErrorPageLayout from '../ErrorPageLayout'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<ErrorPageLayout title="Test title" />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<ErrorPageLayout title="Test title" />).toJSON()

  expect(tree).toMatchSnapshot()
})
