import styled from 'styled-components'
import * as React from 'react'
import Logo from '../images/Logo'
import PublicCover from '../images/PublicCover'
import QuoteContainer from '../ui/QuoteContainer'
import theme, { media, WrappedStyledComponent } from '../../theme'

/**
 *
 * UI components
 *
 */

const DesktopLogo = styled(Logo).attrs({
  light: true,
})`
  display: none;
  ${/* sc-declaration */ media.desktop`
    display: block;
    margin: 39px;
  `}
`

const Aside = styled.aside`
  display: none;
  ${/* sc-declaration */ media.desktop`
    display: block;

    grid-column: 1;
    grid-row: span 2;
    grid-area: aside;
  `}
`

const Header = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  margin-top: 24px;
  grid-area: header;
  ${/* sc-declaration */ media.desktop`
    flex-direction: row;
    justify-content: flex-end;
    align-items: flex-end;
    margin-top: 0;
    grid-column: 2;
  `}
`

const Main = styled.main`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 24px;
  grid-area: main;
  ${/* sc-declaration */ media.desktop`
    justify-content: center;
    margin-bottom: 24px;
    grid-column: 2;
  `}
`

const PublicLayoutContainer = styled.div`
  display: grid;
  grid-template-rows: minmax(114px, 114fr) 567fr;
  grid-template-areas:
    'header'
    'main';
  max-width: 100%;
  width: 100%;
  height: 100%;
  margin: 24px;
  ${/* sc-declaration */ media.desktop`
    margin: 0;
    grid-template-columns: 1fr 3fr;
    grid-template-rows: 104fr 1024fr;
    grid-template-areas:  "aside  header" 
                          "aside  main";
  `}
`

/**
 *
 * Layout for public part of website.
 *
 * Handles rendering of <aside> and renders children in <main>
 * and optional <header> content throught `renderHeader` prop.
 *
 */

interface PublicLayoutProps extends WrappedStyledComponent {
  children?: React.ReactNode
  renderHeader?: () => React.ReactNode
}

const PublicLayout = ({
  className,
  children,
  renderHeader,
}: PublicLayoutProps) => (
  <div className={className}>
    <PublicLayoutContainer>
      <Aside>
        <PublicCover>
          <DesktopLogo />

          <QuoteContainer
            quote={`“Great, kid.
              Don’t get cocky.”`}
            quoteAuthor="Han Solo"
          />
        </PublicCover>
      </Aside>

      <Header>{renderHeader && renderHeader()}</Header>

      <Main>{children}</Main>
    </PublicLayoutContainer>
  </div>
)

const StyledPublicLayout = styled(PublicLayout)`
  max-width: 100%;
  width: 100%;
  height: 100%;
  background-color: ${theme.backgroundColorSecondary};
  display: flex;
`

export default StyledPublicLayout
