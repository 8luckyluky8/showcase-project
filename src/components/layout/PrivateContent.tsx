import styled from 'styled-components'

const PrivateContent = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: stretch;
  align-items: flex-start;
  height: 100%;
  margin-top: 39px;
`

export default PrivateContent
