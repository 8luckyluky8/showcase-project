import * as React from 'react'
import { WrappedStyledComponent } from 'src/theme'
import PublicLayout from './PublicLayout'
import StormTrooperIcon from '../icons/StormTrooperIcon'
import styled from 'styled-components'
import Heading from '../text/Heading'
import PrimaryText from '../text/PrimaryText'
import RefreshLargeButton from '../buttons/RefreshLargeButton'
import PublicHeader from '../ui/PublicHeader'

const StyledStormTrooperIcon = styled(StormTrooperIcon)`
  position: absolute;
  left: -139px;
`

const StyledText = styled(PrimaryText)`
  font-size: 1.125rem;
  margin-top: 6px;
  margin-bottom: 40px;
`

const ErrorPageContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  margin: 0 23%;
  z-index: 1;
`

interface ErrorPageLayout extends WrappedStyledComponent {
  title: string
  children?: React.ReactNode
}

const ErrorPageLayout = ({ title, children }: ErrorPageLayout) => {
  const onRefreshButtonClicked = () => {
    window.location.reload()
  }

  return (
    <PublicLayout renderHeader={PublicHeader}>
      <StyledStormTrooperIcon />

      <ErrorPageContent>
        <Heading>{title}</Heading>

        <StyledText>
          Seems like Darth Vader just hits our website and drops it down.{'\n'}
          Please press the refresh button and everything should be fine again.
        </StyledText>

        <RefreshLargeButton onClick={onRefreshButtonClicked}>
          REFRESH
        </RefreshLargeButton>

        {children}
      </ErrorPageContent>
    </PublicLayout>
  )
}

export default ErrorPageLayout
