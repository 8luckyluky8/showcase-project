import * as React from 'react'
import styled from 'styled-components'
import theme, { media, WrappedStyledComponent } from '../../theme'
import Logo from '../images/Logo'
import { connect } from 'react-redux'
import { State } from '../../store'
import UserMenu from '../ui/user/UserMenu'
import StickyAddIcon from '../icons/StickyAddIcon'
import CloseMenu from '../ui/CloseMenu'

/**
 *
 * UI components
 *
 */

const PrivateLayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  margin: 0 8px;
  max-width: 100%;
  width: 100%;
  height: 100%;
  ${/* sc-declaration */ media.desktop`
    margin: 0 43px;
  `}
`

const Header = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 24px;
  ${/* sc-declaration */ media.desktop`
  margin-top: 31px;
  `}
`

const Main = styled.main`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  height: 100%;
  margin-top: 98px;
  margin-bottom: 98px;
  margin-left: 8px;
  margin-right: 8px;
  ${/* sc-declaration */ media.desktop`
    margin-left: 77px;
    margin-right: 77px;
  `}
`

const PrivateLogo = styled(Logo)`
  margin-left: 16px;
  ${/* sc-declaration */ media.desktop`
    margin-left: 0;
  `}
`

/**
 *
 * Redux connect mappings
 *
 */

const mapState = (state: State) => ({
  user: state.user.user,
})

interface PrivateLayoutProps extends WrappedStyledComponent {
  children?: React.ReactNode
  hideAddEventIcon?: boolean
  showClose?: boolean
  onClose?: () => any
}

/**
 *
 * Layout for private part of website.
 *
 * Handles rendering of <header> and renders children in <main>.
 *
 */

const PrivateLayout = ({
  className,
  children,
  user,
  hideAddEventIcon,
  showClose,
}: PrivateLayoutProps & ReturnType<typeof mapState>) => (
  <div className={className}>
    <PrivateLayoutContainer>
      <Header>
        <PrivateLogo />

        {showClose ? <CloseMenu /> : <UserMenu user={user} />}
      </Header>

      <Main>{children}</Main>
    </PrivateLayoutContainer>

    {!hideAddEventIcon && <StickyAddIcon />}
  </div>
)

export const StyledPrivateLayout = styled(PrivateLayout)`
  width: 100%;
  min-height: 100%;
  background-color: ${theme.backgroundColorPrimary};
  display: flex;
`

export default connect(mapState)(StyledPrivateLayout)
