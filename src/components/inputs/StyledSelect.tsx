import * as React from 'react'
import styled from 'styled-components'
import DropDownIcon from '../icons/DropDownIcon'
import theme from '../../theme'

const SelectIcon = styled(DropDownIcon).attrs({
  color: theme.primaryFontColor,
})`
  position: absolute;
  bottom: 0.7rem;
  right: -0.5rem;
`

const SelectContainer = styled.div`
  overflow: hidden;
  width: 6rem;
  background-color: transparent;
`
const Select = styled.select`
  background: transparent;
  border: none;
  width: 8rem;
  font-family: Hind-Semibold, sans-serif;
  font-size: 0.75rem;
  color: ${theme.primaryFontColor};
  outline: none;
  -webkit-appearance: none;
  margin-left: 0.4rem;
  padding-top: 1px;
`

interface SelectProps
  extends React.DetailedHTMLProps<
    React.SelectHTMLAttributes<HTMLSelectElement>,
    HTMLSelectElement
  > {
  ref?: React.RefObject<HTMLSelectElement>
}

const SelectComponent = ({ className, children, ...rest }: SelectProps) => {
  return (
    <div className={className}>
      <SelectContainer>
        <Select {...rest}>{children}</Select>
      </SelectContainer>

      <SelectIcon />
    </div>
  )
}

const StyledSelect = styled(SelectComponent)`
  position: relative;
`

export default StyledSelect
