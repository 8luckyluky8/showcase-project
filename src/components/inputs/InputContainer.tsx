import styled from 'styled-components'

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-bottom: 44px;
  width: 100%;
`

export default InputContainer
