import * as styledComponents from 'styled-components'
import * as React from 'react'

export interface InputProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  invalid?: boolean
}

export interface InputTheme {}

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<
  InputTheme
>

export { css, createGlobalStyle, keyframes, ThemeProvider }
export default styled
