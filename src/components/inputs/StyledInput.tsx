import * as React from 'react'
import theme, { media } from '../../theme'
import styled from './styled-input'
import ErrorText from '../text/ErrorText'

interface StyledInputProps {
  type?: string
  placeholder?: string
  renderIcon?: () => React.ReactNode
  invalid?: boolean
  errorMessage?: string
  onFocus?: () => any
  value: string
  onChange: (value: string) => any
}

const InputGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: stretch;
  font-family: Hind-Regular, sans-serif;
  width: 100%;
  position: relative;
  ${/* sc-declaration */ media.desktop`
    width: auto;
  `}
`

const InputGroupContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  font-family: Hind-Regular, sans-serif;
  width: 100%;
  ${/* sc-declaration */ media.desktop`
    width: auto;
  `}
`

const Placeholder = styled.span`
  position: absolute;
  pointer-events: none;
  left: 0;
  bottom: 0.3rem;
  transition: 0.2s ease all;
  color: #c9ced3;
`

const movePlaceholderMixin = `
    bottom: 1.8rem;
    color: #d2d6da;
    font-size: 0.875rem;
`

const invalidInputClass = 'input--invalid'

const inputDesktopWidth = '480px'

const Input = styled.input`
  width: 100%;
  height: 33px;
  outline: 0;
  border-width: 0 0 1px;
  border-color: #dae1e7;
  color: ${theme.primaryColor};
  font-size: 1.125rem;
  margin-top: 3rem;
  padding: 0;
  box-shadow: none;

  &.${invalidInputClass} {
    border-color: ${theme.errorColor};
  }

  :focus {
    border-color: ${theme.primaryColor};

    ~ ${/* sc-selector */ Placeholder} {
      ${movePlaceholderMixin}
    }

    &::-webkit-datetime-edit {
      color: ${theme.primaryColor};
    }
  }

  :not(:focus):valid ~ ${/* sc-selector */ Placeholder} {
    ${movePlaceholderMixin}
  }

  :invalid::-webkit-datetime-edit {
    color: transparent;
  }

  ${/* sc-declaration */ media.desktop`
    width: ${inputDesktopWidth};
  `}
`

const ErrorMessage = styled(ErrorText)`
  width: 100%;
  text-align: left;
  ${/* sc-declaration */ media.desktop`
    width: ${inputDesktopWidth};
  `}
`

const StyledInput = ({
  type,
  placeholder,
  value,
  onChange,
  renderIcon,
  invalid,
  onFocus,
  errorMessage,
}: StyledInputProps) => {
  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    onChange((event.target as HTMLInputElement).value)
  }

  return (
    <InputGroupContainer>
      <InputGroup>
        <Input
          required
          className={invalid ? invalidInputClass : ''}
          type={type || 'text'}
          value={value}
          onChange={handleChange}
          onFocus={onFocus}
        />

        {placeholder && <Placeholder>{placeholder}</Placeholder>}

        {renderIcon && renderIcon()}
      </InputGroup>

      {invalid && errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
    </InputGroupContainer>
  )
}

export default StyledInput
