import * as React from 'react'
import { shallow } from 'enzyme'
import StyledSelect from '../StyledSelect'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<StyledSelect />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<StyledSelect />).toJSON()

  expect(tree).toMatchSnapshot()
})
