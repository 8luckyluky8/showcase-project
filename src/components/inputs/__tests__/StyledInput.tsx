import * as React from 'react'
import { shallow } from 'enzyme'
import StyledInput from '../StyledInput'
import * as renderer from 'react-test-renderer'

it('text renders without crashing', () => {
  shallow(<StyledInput value="test" onChange={() => null} />)
})

it('text matches snapshot', () => {
  const tree = renderer
    .create(<StyledInput value="test" onChange={() => null} />)
    .toJSON()

  expect(tree).toMatchSnapshot()
})

it('date renders without crashing', () => {
  shallow(<StyledInput type="date" value="test" onChange={() => null} />)
})

it('date matches snapshot', () => {
  const tree = renderer
    .create(<StyledInput type="date" value="test" onChange={() => null} />)
    .toJSON()

  expect(tree).toMatchSnapshot()
})
