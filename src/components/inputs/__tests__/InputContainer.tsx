import * as React from 'react'
import { shallow } from 'enzyme'
import InputContainer from '../InputContainer'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<InputContainer />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<InputContainer />).toJSON()

  expect(tree).toMatchSnapshot()
})
