import { ButtonProps } from './styled-buttons'
import { useLargeButton } from './LargeButton'
import { positiveButtonTheme } from './PositiveButton'

interface PositiveLargeButtonProps extends ButtonProps {}

const PositiveLargeButton = ({ children, ...rest }: PositiveLargeButtonProps) =>
  useLargeButton({ children, theme: positiveButtonTheme, ...rest })

export default PositiveLargeButton
