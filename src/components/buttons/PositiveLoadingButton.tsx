import { ButtonProps } from './styled-buttons'
import * as React from 'react'
import LoadingIcon from '../icons/LoadingIcon'
import PositiveLargeButton from './PositiveLargeButton'

interface PositiveLoadingButtonProps extends ButtonProps {
  isLoading?: boolean
  text: string
}

const PositiveLoadingButton = ({
  isLoading,
  text,
  ...rest
}: PositiveLoadingButtonProps) => (
  <PositiveLargeButton {...rest}>
    {isLoading ? <LoadingIcon /> : <span>{text}</span>}
  </PositiveLargeButton>
)

export default PositiveLoadingButton
