import * as styledComponents from 'styled-components'
import * as React from 'react'
import { WrappedStyledComponent } from 'src/theme'

export interface ButtonProps
  extends React.ButtonHTMLAttributes<Element>,
    WrappedStyledComponent {
  children?: React.ReactNode
  onClick?: () => any
}

export interface ButtonTheme {
  color: string
  fontColor?: string
  hoverColor?: string
}

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<
  ButtonTheme
>

export { css, createGlobalStyle, keyframes, ThemeProvider }
export default styled
