import { ButtonTheme, ThemeProvider } from './styled-buttons'
import * as React from 'react'
import StyledButton from './StyledButton'
import theme from '../../theme'

export const neutralButtonTheme: ButtonTheme = {
  color: theme.ternaryColor,
  fontColor: theme.inactiveColor,
  hoverColor: '#c4c9d1',
}

interface NeutralButtonProps extends React.DOMAttributes<HTMLButtonElement> {}

const NeutralButton = ({ children, ...rest }: NeutralButtonProps) => (
  <ThemeProvider theme={neutralButtonTheme}>
    <StyledButton {...rest}>{children}</StyledButton>
  </ThemeProvider>
)

export default NeutralButton
