import { ButtonProps } from './styled-buttons'
import { useLargeButton } from './LargeButton'
import { negativeButtonTheme } from './NegativeButton'

interface NegativeLargeButtonProps extends ButtonProps {}

const NegativeLargeButton = ({ children, ...rest }: NegativeLargeButtonProps) =>
  useLargeButton({ children, theme: negativeButtonTheme, ...rest })

export default NegativeLargeButton
