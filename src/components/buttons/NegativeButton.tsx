import { ButtonTheme, ThemeProvider } from './styled-buttons'
import * as React from 'react'
import StyledButton from './StyledButton'
import theme from '../../theme'

export const negativeButtonTheme: ButtonTheme = {
  color: theme.errorColor,
  fontColor: theme.secondaryColor,
  hoverColor: '#e73370',
}

interface NegativeButtonProps extends React.DOMAttributes<HTMLButtonElement> {}

const NegativeButton = ({ children, ...rest }: NegativeButtonProps) => (
  <ThemeProvider theme={negativeButtonTheme}>
    <StyledButton {...rest}>{children}</StyledButton>
  </ThemeProvider>
)

export default NegativeButton
