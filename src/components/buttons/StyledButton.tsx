import styled from './styled-buttons'

const StyledButton = styled.button`
  background-color: ${props => props.theme.color};
  color: ${props => props.theme.fontColor};
  border-radius: 4px;
  text-align: center;
  max-width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 32px;
  border-style: none;
  outline: none;
  cursor: pointer;
  font-size: 0.875rem;

  :hover {
    background-color: ${props => props.theme.hoverColor || props.theme.color};
  }
`

export default StyledButton
