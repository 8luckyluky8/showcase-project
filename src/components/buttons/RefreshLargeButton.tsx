import { ButtonProps, ButtonTheme } from './styled-buttons'
import { useLargeButton } from './LargeButton'
import theme from '../../theme'

export const refreshButtonTheme: ButtonTheme = {
  color: theme.primaryColor,
  fontColor: theme.secondaryColor,
  hoverColor: theme.primaryHoverColor,
}

interface RefreshLargeButtonProps extends ButtonProps {}

const RefreshLargeButton = ({ children, onClick }: RefreshLargeButtonProps) =>
  useLargeButton({ children, onClick, theme: refreshButtonTheme })

export default RefreshLargeButton
