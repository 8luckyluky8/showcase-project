import * as React from 'react'
import { shallow } from 'enzyme'
import NeutralButton from '../NeutralButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<NeutralButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<NeutralButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
