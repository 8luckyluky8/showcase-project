import * as React from 'react'
import { shallow } from 'enzyme'
import LargeButton from '../LargeButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<LargeButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<LargeButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
