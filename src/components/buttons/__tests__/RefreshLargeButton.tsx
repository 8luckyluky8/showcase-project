import * as React from 'react'
import { shallow } from 'enzyme'
import RefreshLargeButton from '../RefreshLargeButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<RefreshLargeButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<RefreshLargeButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
