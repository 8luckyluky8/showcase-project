import * as React from 'react'
import { shallow } from 'enzyme'
import PositiveLoadingButton from '../PositiveLoadingButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PositiveLoadingButton text="TEST" />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PositiveLoadingButton text="TEST" />).toJSON()

  expect(tree).toMatchSnapshot()
})

it('loading renders without crashing', () => {
  shallow(<PositiveLoadingButton text="TEST" isLoading />)
})

it('loading matches snapshot', () => {
  const tree = renderer
    .create(<PositiveLoadingButton text="TEST" isLoading />)
    .toJSON()

  expect(tree).toMatchSnapshot()
})
