import * as React from 'react'
import { shallow } from 'enzyme'
import PositiveButton from '../PositiveButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PositiveButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PositiveButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
