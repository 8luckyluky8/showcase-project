import * as React from 'react'
import { shallow } from 'enzyme'
import PositiveLargeButton from '../PositiveLargeButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PositiveLargeButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PositiveLargeButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
