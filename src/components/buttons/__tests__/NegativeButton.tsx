import * as React from 'react'
import { shallow } from 'enzyme'
import NegativeButton from '../NegativeButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<NegativeButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<NegativeButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
