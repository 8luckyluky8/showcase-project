import * as React from 'react'
import { shallow } from 'enzyme'
import NegativeLargeButton from '../NegativeLargeButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<NegativeLargeButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<NegativeLargeButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
