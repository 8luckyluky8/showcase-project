import * as React from 'react'
import { shallow } from 'enzyme'
import StyledButton from '../StyledButton'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<StyledButton />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<StyledButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
