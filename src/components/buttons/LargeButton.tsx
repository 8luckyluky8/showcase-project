import * as React from 'react'
import StyledButton from './StyledButton'
import styled, {
  ButtonProps,
  ThemeProvider,
  ButtonTheme,
} from './styled-buttons'
import { media } from '../../theme'

export interface LargeButtonProps extends ButtonProps {
  theme: ButtonTheme
}

const LargeButton = styled(StyledButton)`
  width: 90%;
  height: 57px;
  font-family: Hind-SemiBold, sans-serif;
  font-size: 1rem;
  ${/* sc-declaration */ media.desktop`
    width: 240px;
  `}
`

export const useLargeButton = ({
  children,
  theme,
  ...rest
}: LargeButtonProps) => (
  <ThemeProvider theme={theme}>
    <LargeButton {...rest}>{children}</LargeButton>
  </ThemeProvider>
)

export default LargeButton
