import { ButtonTheme, ThemeProvider } from './styled-buttons'
import * as React from 'react'
import StyledButton from './StyledButton'
import theme from '../../theme'

export const positiveButtonTheme: ButtonTheme = {
  color: '#22d486',
  fontColor: theme.secondaryColor,
  hoverColor: '#20bd78',
}

interface PositiveButtonProps extends React.DOMAttributes<HTMLButtonElement> {}

const PositiveButton = ({ onClick, children }: PositiveButtonProps) => (
  <ThemeProvider theme={positiveButtonTheme}>
    <StyledButton onClick={onClick}>{children}</StyledButton>
  </ThemeProvider>
)

export default PositiveButton
