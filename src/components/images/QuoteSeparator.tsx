import styled from 'styled-components'

const QuoteSeparator = styled.div`
  background-color: #1be38b;
  width: 12px;
  height: 2px;
  align-self: center;
  margin-top: 21px;
  margin-bottom: 21px;
`

export default QuoteSeparator
