import styled from 'styled-components'
import theme from '../../theme'
import publicCoverImg from '../../assets/images/public-cover.png'

const PublicCover = styled.div`
  background-image: url(${publicCoverImg});
  background-size: cover;
  background-color: ${theme.primaryColor};
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export default PublicCover
