import * as React from 'react'
import { shallow } from 'enzyme'
import PublicCover from '../PublicCover'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<PublicCover />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<PublicCover />).toJSON()

  expect(tree).toMatchSnapshot()
})
