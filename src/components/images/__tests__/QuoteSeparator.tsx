import * as React from 'react'
import { shallow } from 'enzyme'
import QuoteSeparator from '../QuoteSeparator'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<QuoteSeparator />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<QuoteSeparator />).toJSON()

  expect(tree).toMatchSnapshot()
})
