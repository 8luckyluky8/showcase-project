import * as React from 'react'
import { shallow } from 'enzyme'
import Logo from '../Logo'
import * as renderer from 'react-test-renderer'

it('renders without crashing', () => {
  shallow(<Logo />)
})

it('matches snapshot', () => {
  const tree = renderer.create(<Logo />).toJSON()

  expect(tree).toMatchSnapshot()
})

it('light renders without crashing', () => {
  shallow(<Logo light />)
})

it('light matches snapshot', () => {
  const tree = renderer.create(<Logo light />).toJSON()

  expect(tree).toMatchSnapshot()
})
