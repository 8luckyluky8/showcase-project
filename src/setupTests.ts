import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import { User } from './models/user'
import { createMemoryHistory } from 'history'

Enzyme.configure({ adapter: new Adapter() })

export const testUser: User = {
  id: 1,
  firstName: 'Foo',
  lastName: 'Bar',
  email: 'foo@bar',
  authToken: '',
  refreshToken: '',
}

export const history = createMemoryHistory()
export const location = {
  pathname: '',
  search: '',
  state: null,
  hash: '',
}
export const match = {
  params: [],
  isExact: false,
  path: '',
  url: '',
}
