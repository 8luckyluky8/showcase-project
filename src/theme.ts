import { css } from 'styled-components'

export interface WrappedStyledComponent {
  className?: string
}

export default {
  backgroundColorPrimary: '#f9f9fb',
  backgroundColorSecondary: '#ffffff',
  primaryColor: '#323c46',
  secondaryColor: '#ffffff',
  ternaryColor: '#d9dce1',
  inactiveColor: '#a9aeb4',
  errorColor: '#ff4081',
  primaryFontColor: '#323c46',
  secondaryFontColor: '#949ea8',
  ternaryFontColor: '#cacdd0',
  primaryHoverColor: '#565d5a',
}

const sizes = {
  desktop: 794,
  largeDesktop: 1030,
}

export const media = Object.keys(sizes).reduce(
  (acc, label) => {
    acc[label] = (literals: TemplateStringsArray, ...placeholders: any[]) =>
      css`
        @media (min-width: ${sizes[label]}px) {
          ${css(literals, ...placeholders)};
        }
      `
    return acc
  },
  {} as Record<
    keyof typeof sizes,
    (l: TemplateStringsArray, ...p: any[]) => string
  >
)
