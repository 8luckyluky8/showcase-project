import { createModel } from '@rematch/core'

export interface User {
  id: number
  firstName: string
  lastName: string
  email: string
  authToken: string
  refreshToken: string
}

export interface UserState {
  user: User
  isAuthenticated: boolean
}

const user = createModel<UserState>({
  state: {
    user: null,
    isAuthenticated: false,
  },
  reducers: {
    setUser: (state: UserState, user: User): UserState => ({
      ...state,
      user,
    }),
    setUserAuthenticated: (state: UserState, isAuthenticated: boolean) => ({
      ...state,
      isAuthenticated,
    }),
    setLoggedInUser: (state: UserState, user: User) => ({
      ...state,
      user,
      isAuthenticated: true,
    }),
    clearUser: (state: UserState) => ({
      ...state,
      user: null,
      isAuthenticated: false,
    }),
  },
  effects: {},
})

export default user
