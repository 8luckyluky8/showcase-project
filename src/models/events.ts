import { User } from './user'
import { createModel } from '@rematch/core'
import {
  sendListAllEventsRequest,
  AllEventsResponse,
  sendAttendEventRequest,
  AttendEventSuccessResponse,
  sendUnattendEventRequest,
  sendCreateEventRequest,
  CreateEventSuccessResponse,
  UnattendEventSuccessResponse,
  sendUpdateEventRequest,
} from 'src/api/events'

export enum EventsFilters {
  AllEvents,
  FutureEvents,
  PastEvents,
}

export const eventFiltersValues: number[] = Object.keys(EventsFilters)
  .filter(key => isNaN(Number(EventsFilters[key])))
  .map(value => Number(value))

export interface EventFormData {
  title: string
  description: string
  startsAt: string
  capacity: number
}

export interface EventData extends EventFormData {
  id: number
  owner: User
  attendees: User[]
  createdAt: string
  updatedAt: string
}

export interface EventsState {
  events: EventData[]
  futureEvents: EventData[]
  pastEvents: EventData[]
  filterType: EventsFilters
}

const events = createModel({
  state: {
    events: [],
    futureEvents: [],
    pastEvents: [],
    filterType: EventsFilters.AllEvents,
  },
  reducers: {
    setEvents: (state: EventsState, events: EventData[]) => ({
      ...state,
      events,
    }),
    setFutureEvents: (state: EventsState, futureEvents: EventData[]) => ({
      ...state,
      futureEvents,
    }),
    setPastEvents: (state: EventsState, pastEvents: EventData[]) => ({
      ...state,
      pastEvents,
    }),
    setFilterType: (state: EventsState, filterType: EventsFilters) => ({
      ...state,
      filterType,
    }),
    updateEvent: (state: EventsState, updatedEvent: EventData) => {
      const events = state.events.map((event: EventData) => {
        if (event.id !== updatedEvent.id) {
          return event
        }

        return updatedEvent
      })

      return {
        ...state,
        events,
      }
    },
  },
  effects: {
    async loadEvents() {
      const events: AllEventsResponse = await sendListAllEventsRequest()
      this.setEvents(events)
      this.setPastEvents(
        events.filter(
          (event: EventData) => new Date(event.startsAt) < new Date()
        )
      )
      this.setFutureEvents(
        events.filter(
          (event: EventData) => new Date(event.startsAt) > new Date()
        )
      )
    },
    async attendEvent({ eventId }: { eventId: EventData['id'] }) {
      const updatedEventResponse = await sendAttendEventRequest(eventId)
      if ((updatedEventResponse as AttendEventSuccessResponse).id) {
        this.updateEvent(updatedEventResponse)
      } else {
        // @TODO: Show error message
      }
    },
    async unattendEvent({ eventId }: { eventId: EventData['id'] }) {
      const updatedEventResponse = await sendUnattendEventRequest(eventId)
      if ((updatedEventResponse as UnattendEventSuccessResponse).id) {
        this.updateEvent(updatedEventResponse)
      } else {
        // @TODO: Show error message
      }
    },
    async createEvent({ eventFormData }: { eventFormData: EventFormData }) {
      const createEventResponse = await sendCreateEventRequest(eventFormData)

      this.updateEvent(createEventResponse as CreateEventSuccessResponse)
    },
    async editEvent({
      eventId,
      eventFormData,
    }: {
      eventId: number
      eventFormData: EventFormData
    }) {
      const createEventResponse = await sendUpdateEventRequest(
        eventId,
        eventFormData
      )

      this.updateEvent(createEventResponse as CreateEventSuccessResponse)
    },
  },
})

export default events
