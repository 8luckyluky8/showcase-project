import * as React from 'react'
import Card from 'src/components/ui/Card'
import PrivateLayout from 'src/components/layout/PrivateLayout'
import EventForm from 'src/components/forms/EventForm'
import { EventFormData, EventData } from 'src/models/events'
import { Dispatch, State } from 'src/store'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import ErrorPageScreen from './ErrorPageScreen'

/**
 *
 * Redux connect mappings
 *
 */
const mapState = (state: State) => ({
  events: state.events.events,
})

const mapDispatch = (dispatch: Dispatch) => ({
  editEvent: (eventId: EventData['id'], eventFormData: EventFormData) =>
    dispatch.events.editEvent({
      eventId,
      eventFormData,
    }),
})

type EditEventScreenProps = ReturnType<typeof mapState> &
  ReturnType<typeof mapDispatch> &
  RouteComponentProps<{ id: string }>

const EditEventScreen = ({
  events,
  editEvent,
  history,
  match,
}: EditEventScreenProps) => {
  const eventId: EventData['id'] = Number(match.params.id)

  const eventData = events.find((event: EventData) => event.id === eventId)

  const [isLoading, setIsLoading] = React.useState(false)
  const [hasError, setHasError] = React.useState(false)

  const onFormSubmit = async (eventFormData: EventFormData) => {
    try {
      setIsLoading(true)
      await editEvent(eventId, eventFormData)

      history.goBack()
    } catch {
      setHasError(true)
    } finally {
      setIsLoading(false)
    }
  }

  if (hasError) {
    return <ErrorPageScreen />
  }

  return (
    <PrivateLayout hideAddEventIcon showClose>
      <Card>
        <EventForm
          onSubmit={onFormSubmit}
          isLoading={isLoading}
          defaultData={eventData}
        />
      </Card>
    </PrivateLayout>
  )
}

export default connect(
  mapState,
  mapDispatch
)(EditEventScreen)
