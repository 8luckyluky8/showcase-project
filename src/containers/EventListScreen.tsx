import * as React from 'react'
import { State, Dispatch } from 'src/store'
import { connect } from 'react-redux'
import PrivateLayout from 'src/components/layout/PrivateLayout'
import StyledFilter from 'src/components/ui/StyledFilter'
import PrivateContent from 'src/components/layout/PrivateContent'
import { EventsFilters, eventFiltersValues, EventData } from 'src/models/events'
import { Action } from '@rematch/core'
import EventList from 'src/components/ui/events/EventList'
import { RouteComponentProps, Route, Switch, Redirect } from 'react-router'
import CenteredContent from 'src/components/layout/CenteredContent'
import LoadingIcon from 'src/components/icons/LoadingIcon'
import theme from '../theme'
import styled from 'styled-components'
import ViewFilter, { ViewFilterType } from 'src/components/ui/ViewFilter'
import AppRoutes from 'src/appRoutes'
import ErrorPageScreen from './ErrorPageScreen'

/**
 *
 * UI components
 *
 */

const StyledLoadingIcon = styled(LoadingIcon)`
  width: 50px;
  height: 50px;
`
const FilterRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

/**
 *
 * Redux connect mappings
 *
 */

const mapState = (state: State) => ({
  events: state.events.events,
  futureEvents: state.events.futureEvents,
  pastEvents: state.events.pastEvents,
  eventsFilterType: state.events.filterType,
  user: state.user.user,
})

const mapDispatch = (dispatch: Dispatch) => ({
  loadEvents: () => dispatch.events.loadEvents(),
  // Typecasting because of rematch TS bug fix
  setEventsFilterType: (filterType: EventsFilters) =>
    dispatch.events.setFilterType((filterType as unknown) as Action<
      EventsFilters
    >),
  attendEvent: (eventId: EventData['id']) =>
    dispatch.events.attendEvent({ eventId }),
  unattendEvent: (eventId: EventData['id']) =>
    dispatch.events.unattendEvent({ eventId }),
})

/**
 *
 * Event list screen container.
 * Renders events (AppRoutes.DEFAULT)
 *
 */

const filterText = {
  [EventsFilters.AllEvents]: 'ALL EVENTS',
  [EventsFilters.FutureEvents]: 'FUTURE EVENTS',
  [EventsFilters.PastEvents]: 'PAST EVENTS',
}

const filterRoutes = {
  [EventsFilters.AllEvents]: AppRoutes.Events,
  [EventsFilters.FutureEvents]: AppRoutes.FutureEvents,
  [EventsFilters.PastEvents]: AppRoutes.PastEvents,
}

type EventListScreenProps = ReturnType<typeof mapState> &
  ReturnType<typeof mapDispatch> &
  RouteComponentProps<{}>

interface EventListScreenState {
  isLoading: boolean
  viewFilterType: ViewFilterType
  hasError: boolean
}

class EventListScreen extends React.Component<
  EventListScreenProps,
  EventListScreenState
> {
  state = {
    isLoading: true,
    viewFilterType: ViewFilterType.GridView,
    hasError: false,
  }

  async componentDidMount() {
    try {
      await this.props.loadEvents()

      // Set correct filter type based on url in case of refresh
      const pathName = this.props.location.pathname.substr(1)
      const compareRouteToPathName = (route: string): boolean =>
        (route && pathName.startsWith(route)) ||
        (!route && pathName === `${filterRoutes[EventsFilters.AllEvents]}`)

      if (!compareRouteToPathName(filterRoutes[this.props.eventsFilterType])) {
        const eventFilterType = Object.keys(filterRoutes).find(filterRouteKey =>
          compareRouteToPathName(filterRoutes[filterRouteKey])
        )

        this.props.setEventsFilterType(
          Number(eventFilterType) || EventsFilters.AllEvents
        )
      }
    } catch {
      this.setState({
        hasError: true,
      })
    } finally {
      this.setState({
        isLoading: false,
      })
    }
  }

  onViewFilterChange = (filterType: ViewFilterType) => {
    this.setState({
      viewFilterType: filterType,
    })
  }

  onAttendEvent = (eventId: EventData['id']) => {
    this.props.attendEvent(eventId)
  }

  onUnattendEvent = (eventId: EventData['id']) => {
    this.props.unattendEvent(eventId)
  }

  onEditEvent = (eventId: EventData['id']) => {
    this.props.history.push(`${AppRoutes.EditEvent}/${eventId}`)
  }

  renderEventList = (events: EventData[]) => (
    <EventList
      events={events}
      viewFilterType={this.state.viewFilterType}
      user={this.props.user}
      onAttendEvent={this.onAttendEvent}
      onUnattendEvent={this.onUnattendEvent}
      onEditEvent={this.onEditEvent}
    />
  )

  render() {
    if (this.state.hasError) {
      return <ErrorPageScreen />
    }

    return (
      <PrivateLayout>
        <FilterRow>
          <StyledFilter
            selectedFilterValue={`${this.props.eventsFilterType}`}
            onFilterSelect={(selectedValue: string) => {
              const filterType = Number(selectedValue)
              this.props.setEventsFilterType(filterType)
              this.props.history.push(filterRoutes[filterType])
            }}
            filterOptions={eventFiltersValues.map(
              (eventFiltersValue: number) => ({
                value: `${eventFiltersValue}`,
                label: filterText[eventFiltersValue],
              })
            )}
          />

          <ViewFilter
            activeFilter={this.state.viewFilterType}
            onFilterChange={this.onViewFilterChange}
          />
        </FilterRow>

        {this.state.isLoading ? (
          <CenteredContent>
            <StyledLoadingIcon color={theme.inactiveColor} />
          </CenteredContent>
        ) : (
          <PrivateContent>
            <Switch>
              <Route
                path={filterRoutes[EventsFilters.FutureEvents]}
                render={() => this.renderEventList(this.props.futureEvents)}
              />

              <Route
                path={filterRoutes[EventsFilters.PastEvents]}
                render={() => this.renderEventList(this.props.pastEvents)}
              />

              <Route
                path={filterRoutes[EventsFilters.AllEvents]}
                render={() => this.renderEventList(this.props.events)}
              />
            </Switch>

            <Redirect from={AppRoutes.Default} exact to={AppRoutes.Events} />
          </PrivateContent>
        )}
      </PrivateLayout>
    )
  }
}

export default connect(
  mapState,
  mapDispatch
)(EventListScreen)
