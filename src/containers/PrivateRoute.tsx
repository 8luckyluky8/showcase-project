import * as React from 'react'
import { Route, Redirect, RouteComponentProps } from 'react-router-dom'
import { WrappedRouteProps } from 'src/AppRouter'
import { connect } from 'react-redux'
import { State } from 'src/store'
import AppRoutes from 'src/appRoutes'

const mapState = (state: State) => ({
  isUserAuthenticated: state.user.isAuthenticated,
})

type PrivateRouteProps = ReturnType<typeof mapState> & WrappedRouteProps

const PrivateRoute = ({
  isUserAuthenticated,
  component: Component,
  ...rest
}: PrivateRouteProps) => (
  <Route
    {...rest}
    render={(props: RouteComponentProps<any>) =>
      isUserAuthenticated ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: AppRoutes.SignIn,
            state: { from: props.location },
          }}
        />
      )
    }
  />
)

export default connect(mapState)(PrivateRoute)
