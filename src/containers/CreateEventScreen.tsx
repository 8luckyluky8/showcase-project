import * as React from 'react'
import Card from 'src/components/ui/Card'
import PrivateLayout from 'src/components/layout/PrivateLayout'
import EventForm from 'src/components/forms/EventForm'
import { EventFormData } from 'src/models/events'
import { Dispatch } from 'src/store'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import ErrorPageScreen from './ErrorPageScreen'

/**
 *
 * Redux connect mappings
 *
 */

const mapDispatch = (dispatch: Dispatch) => ({
  createEvent: (eventFormData: EventFormData) =>
    dispatch.events.createEvent({
      eventFormData,
    }),
})

type CreateEventScreenProps = ReturnType<typeof mapDispatch> &
  RouteComponentProps

const CreateEventScreen = ({
  createEvent,
  history,
}: CreateEventScreenProps) => {
  const [isLoading, setIsLoading] = React.useState(false)
  const [hasError, setHasError] = React.useState(false)

  const onFormSubmit = async (eventFormData: EventFormData) => {
    try {
      setIsLoading(true)
      await createEvent(eventFormData)

      history.goBack()
    } catch {
      setHasError(true)
    } finally {
      setIsLoading(false)
    }
  }

  if (hasError) {
    return <ErrorPageScreen />
  }

  return (
    <PrivateLayout hideAddEventIcon showClose>
      <Card>
        <EventForm onSubmit={onFormSubmit} isLoading={isLoading} />
      </Card>
    </PrivateLayout>
  )
}

export default connect(
  null,
  mapDispatch
)(CreateEventScreen)
