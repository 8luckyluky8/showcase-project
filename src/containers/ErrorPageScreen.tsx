import * as React from 'react'
import ErrorPageLayout from '../components/layout/ErrorPageLayout'

const ErrorPageScreen = () => <ErrorPageLayout title="Something went wrong" />

export default ErrorPageScreen
