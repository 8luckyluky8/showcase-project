import * as React from 'react'
import ErrorPageLayout from 'src/components/layout/ErrorPageLayout'

const PageNotFoundScreen = () => (
  <ErrorPageLayout title="404 Error - page not found" />
)

export default PageNotFoundScreen
