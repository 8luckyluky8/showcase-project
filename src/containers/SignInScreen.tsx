import * as React from 'react'
import PublicLayout from 'src/components/layout/PublicLayout'
import SignInForm from 'src/components/forms/SignInForm'
import { connect } from 'react-redux'
import { Dispatch, State } from 'src/store'
import { User } from 'src/models/user'
import { Redirect } from 'react-router-dom'
import { Location } from 'history'
import PublicHeader from 'src/components/ui/PublicHeader'
import AppRoutes from 'src/appRoutes'
import ErrorPageScreen from './ErrorPageScreen'

/**
 *
 * Redux connect mappings
 *
 */

const mapState = (state: State) => ({
  isUserAuthenticated: state.user.isAuthenticated,
})

const mapDispatch = (dispatch: Dispatch) => ({
  setLoggedInUser: (user: User) => dispatch.user.setLoggedInUser(user),
})

/**
 *
 * Login screen container.
 * Renders login screen page (AppRoutes.SignIn)
 *
 */

type LoginScreenProps = ReturnType<typeof mapState> &
  ReturnType<typeof mapDispatch> & { location: Location }

const LoginScreen = ({
  setLoggedInUser,
  location,
  isUserAuthenticated,
}: LoginScreenProps) => {
  const [hasError, setHasError] = React.useState(false)

  const onSuccessfulLogin = (user: User) => {
    setLoggedInUser(user)
  }

  const onError = () => {
    setHasError(true)
  }

  const redirectTo =
    (location.state && location.state.from) || AppRoutes.Default

  if (hasError) {
    return <ErrorPageScreen />
  }

  return (
    <PublicLayout renderHeader={PublicHeader}>
      <SignInForm onLoggedIn={onSuccessfulLogin} onError={onError} />

      {isUserAuthenticated && <Redirect to={redirectTo} />}
    </PublicLayout>
  )
}

export default connect(
  mapState,
  mapDispatch
)(LoginScreen)
