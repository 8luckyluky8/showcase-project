export const formatDate = (dateString: string) => {
  const date = new Date(dateString)

  const formatedDate = date.toLocaleDateString('en-US', {
    month: 'long',
    day: 'numeric',
    year: 'numeric',
  })

  const formatedTime = date.toLocaleString('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true,
  })

  return `${formatedDate} - ${formatedTime}`
}
