enum AppRoutes {
  Default = '/',
  Events = '/events',
  FutureEvents = '/future-events',
  PastEvents = '/past-events',
  SignIn = '/sign-in',
  AddEvent = '/add-event',
  EditEvent = '/edit-event',
  SignUp = '/sign-up',
}

export default AppRoutes
