import * as React from 'react'
import SignInScreen from './containers/SignInScreen'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  RouteProps,
} from 'react-router-dom'
import EventListScreen from './containers/EventListScreen'
import PrivateRoute from './containers/PrivateRoute'
import PageNotFoundScreen from './containers/PageNotFoundScreen'
import AppRoutes from './appRoutes'
import CreateEventScreen from './containers/CreateEventScreen'
import EditEventScreen from './containers/EditEventScreen'

export interface WrappedRouteProps extends RouteProps {
  component: React.ComponentClass<any> | React.FunctionComponent<any>
}

class AppRouter extends React.Component {
  public render() {
    return (
      <Router>
        <Switch>
          <Route path={AppRoutes.SignIn} component={SignInScreen} />

          <PrivateRoute
            path={AppRoutes.Default}
            exact
            component={EventListScreen}
          />
          <PrivateRoute
            path={[
              AppRoutes.Events,
              AppRoutes.FutureEvents,
              AppRoutes.PastEvents,
            ]}
            component={EventListScreen}
          />
          <PrivateRoute
            path={AppRoutes.AddEvent}
            component={CreateEventScreen}
          />
          <PrivateRoute
            path={`${AppRoutes.EditEvent}/:id`}
            component={EditEventScreen}
          />

          <Route component={PageNotFoundScreen} />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter
