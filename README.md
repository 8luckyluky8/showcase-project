## Description

**Work in progress**

Eventio is a web app that allows registered users to sign up for and create events.

The app contains several pages: Login, Sign Up, Reset Password, Event List, Event Detail, Create Event, My Profile.

## Project setup

- `yarn install` - installs dependencies _(run if you have fresh copy)_
- `yarn start` - runs development version
- `yarn build` - builds production version
- `yarn test` - runs unit tests

## API

_Work in progress_

Currently using mocking.

## Done

### Login and Authorized State

Test accounts:

| Username    | Password |
| ----------- | -------- |
| foo@bar.com | 12345    |

### Event list

### Create event

### Edit event

### 404 page

### 50x page

## To Do

### Sign Up

### Reset Password

### My Profile

### Event Detail

### Designs for date and time inputs

### Tests for components with hooks

There are already prepared tests for components with hooks, but parts of them are waiting for fix of shallow render in Jest or Enzyme.
